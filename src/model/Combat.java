package model;

import java.util.Random;

import model.livings.Player;
import server.MUDServer;

public class Combat implements Runnable{
	private Living liv1;
	private Living liv2;
	private MUDServer serv;
	private boolean inCombat ;

	public Combat(MUDServer serv, Living player, String other) {
		this.serv = serv;
		this.liv1 = player;
		this.liv2 = player.getRoom().getLiving(other);
		this.inCombat = true;
		
		liv1.setInCombat(true);
		liv2.setInCombat(true);
		Thread t = new Thread(this);
		t.start();
	}
	
	public boolean hasPlayer(Living player){
		if(player.getID().equals(liv1.getID()) || player.getID().equals(liv2.getID())){
			return true;
		}
		return false;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random random = new Random();
		int hitChance = 0;
		int damage = 0;
		long waitTime = 0;
		long endTime = 0;
		
		while(inCombat){
			
			//liv1 atk on liv2
			hitChance = liv1.getAccuracy()-liv2.getAgility();//calculate hit chance
			
			if(Math.abs(random.nextInt()%100) <= hitChance){
				damage = liv1.getAttack() - liv2.getDefense();//calculate damage
				damage += random.nextInt()%5;
				
				if(damage < 1){
					damage = 1;
				}
				
				liv2.setHP(liv2.getHP() - damage);//do damage
				
//				serv.send message to both players of the damage done
//				System.out.println(liv1.getID() +" hit "+ liv2.getID() +" for "+ damage +" damage");
				serv.sendToPlayer(liv1.getID(), liv1.getID() +" hit "+ liv2.getID() +" for "+ damage +" damage");
				serv.sendToPlayer(liv2.getID(), liv1.getID() +" hit "+ liv2.getID() +" for "+ damage +" damage");
				
				if(liv2.getHP() < 1){//end fight if someone died
					dead(liv2);
					break;
				}
			}
			else{
//				serv.send message that the attack missed
//				System.out.println(liv1.getID() +" attack has missed");
				serv.sendToPlayer(liv1.getID(), liv1.getID() +" attack has missed");
				serv.sendToPlayer(liv2.getID(), liv1.getID() +" attack has missed");
				
			}
			
			
			//liv2 atk on liv1
			hitChance = liv2.getAccuracy()-liv1.getAgility();//calculate hit chance
			
			if(Math.abs(random.nextInt()%100) <= hitChance){
				damage = liv2.getAttack() - liv1.getDefense();//calculate damage
				damage += random.nextInt()%5;
				
				if(damage < 1){
					damage = 1;
				}
				
				liv1.setHP(liv1.getHP() - damage);//do damage
				
//				serv.send message to both players of the damage done
//				System.out.println(liv2.getID() +" hit "+ liv1.getID() +" for "+ damage +" damage");
				serv.sendToPlayer(liv2.getID(), liv2.getID() +" hit "+ liv1.getID() +" for "+ damage +" damage");
				serv.sendToPlayer(liv1.getID(), liv2.getID() +" hit "+ liv1.getID() +" for "+ damage +" damage");
				
				
				if(liv1.getHP() < 1){//end fight if someone died
					dead(liv1);
					break;
				}
			}
			else{
//				serv.send message that the attack missed
//				System.out.println(liv2.getID() +" attack has missed");
				serv.sendToPlayer(liv1.getID(), liv2.getID() +" attack has missed");
				serv.sendToPlayer(liv2.getID(), liv2.getID() +" attack has missed");
				
			}
			
			
			waitTime = System.nanoTime();//waits for one second between attacks by each living
			endTime = waitTime + 1000000000;
			
			while(waitTime < endTime){
				waitTime = System.nanoTime();
			}
			
			if(liv1.getRoom() != liv2.getRoom()){
				endCombat();
			}
		}
//		serv.sendToPlayer(String name, String msg);
		
		inCombat = false;
		liv1.setInCombat(false);
		liv2.setInCombat(false);
	}

	private void dead(Living player) {
		System.out.println(player.getID() +" has diyed");
		
		for(int a=0; a<player.getInventory().size(); a++){//adds all items to the current room
			player.getRoom().addItem((Item) player.getInventory().get(a));
		}
		
		if(player instanceof Player){
			serv.theGame.respan(player);
			
			serv.sendToPlayer(player.getID(), "You have died.");
			if(player.getID().equalsIgnoreCase(liv1.getID())){
				serv.sendToPlayer(liv2.getID(), "You have killed "+ liv1.getID());
			}else{
				serv.sendToPlayer(liv1.getID(), "You have killed "+ liv2.getID());
			}
			
		}else{
			player.getRoom().removeLiving(player);
			
			if(player.getID().equalsIgnoreCase(liv1.getID())){
				serv.sendToPlayer(liv2.getID(), "You have killed "+ liv1.getID());
			}else{
				serv.sendToPlayer(liv1.getID(), "You have killed "+ liv2.getID());
			}
		}
	}

	public void endCombat() {
		inCombat = false;
	}
	
	

}
