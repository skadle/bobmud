package model;

import game.AllLivings;

import java.awt.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import model.items.GoldPouch;
import model.livings.Player;

public abstract class Living {
	protected LinkedList<Item> items = new LinkedList<Item>();
	protected Vector<String> tags = new Vector<String>();
	protected List commands = new List();
	protected String name;
	protected String shortName;
	protected String desc;
	protected int vision;
	protected String id;
	protected int accuracy;
	protected int agility;
	protected int hp;
	protected int maxHp = 100;
	protected int defense;
	protected int attack;
	protected Room room;
	private boolean inCombat;
	private boolean rightHandEquip;
	private boolean leftHandEquip;
	private boolean chestEquip;
	private boolean feetEquip;
	private boolean legEquip;
	private boolean headEquip;
	private boolean admin;
	protected LinkedList<Item> equipped = new LinkedList<Item>();
	
	public Living(String id, Room room) {
		this.id = id;
		this.name = id;
		this.room = room;
		setDefaults();
	}

	protected void setDefaults() {
		AllLivings.add(this);
//		name = "A generic living name is here.";
		shortName = "generic living shortName";
		desc = "    This is a generic placeholder for a living being's desc.";
		hp = 100;
		vision = 0;
		defense = 0;
		attack = 1;
		agility = 20;
		accuracy = 100;
		tags.add(shortName);
		inCombat = false;
		admin = false;
		rightHandEquip = false;
		leftHandEquip = false;
		chestEquip = false;
		feetEquip = false;
		legEquip = false;
		headEquip = false;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setAdmin(boolean temp){
		this.admin = temp;
	}
	
	public boolean isAdmin(){
		return this.admin;
	}

	public boolean hasTag(String str) {
		return tags.contains(str);
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setHP(int hp) {
		this.hp = hp;
	}

	public void changeHP(int x) {
		if (maxHp < 100) {
			if ((maxHp + 5) < 100) {
				hp = 100;
			} else {
				hp += x;
			}
		}
	}

	public int getHP() {
		return hp;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getDefense() {
		return defense;
	}

	public void changeDefense(int extraDefense) {
		defense += extraDefense;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getAttack() {
		return attack;
	}

	public void changeAttack(int extraAttack) {
		attack += extraAttack;
	}

	public void setAccuracy(int accu) {
		this.accuracy = accu;
	}

	public void changeAccuracy(int x) {
		accuracy += x;
	}

	public int getAccuracy() {
		return accuracy;
	}
	
	public void setAgility(int agility) {
		this.agility = agility;
	}

	public void changeAgility(int extraAgility) {
		agility += extraAgility;
	}

	public int getAgility() {
		return agility;
	}

	public boolean getInCombat(){
		return this.inCombat;
	}
	
	public void setInCombat(boolean cb){
		this.inCombat = cb;
	}
	
	public String moveRoom(String direction) {
		if (room.possibleExit(direction)) {
			// need to edit this part
			this.room = room.move(direction, this);

			return room.toString();
		} else {
			return "The direction you have is not valid option";
		}

	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Room getRoom() {
		return room;
	}

	// the abstract chatter method is for things that the room does
	public abstract void chatter();

	public String drop(String item) {
		return dropHelper(item);
	}

	private String dropHelper(String item) {
		try{
			for (int a = 0; a < items.size(); a++) {
				if (items.get(a).getID().equalsIgnoreCase(item)) {
					for(int b = 0; b < equipped.size(); b++){
						if(equipped.get(b).getID().equalsIgnoreCase(items.get(a).getID())){
							room.addItem(items.get(a));
							items.remove(items.get(a));
							this.unEquip(item);
							return item + " has been unequipped and removed from inventory";
						} else if(equipped.get(b).getName().equalsIgnoreCase(items.get(a).getID())){
							room.addItem(items.get(a));
							items.remove(items.get(a));
							this.unEquip(item);
							return item + " has been unequipped and removed from inventory";
						}
					}
					room.addItem(items.get(a));
					items.remove(items.get(a));
					return item + " has been removed from inventory";
				} else if (items.get(a).getID().equalsIgnoreCase(item)) {
					for(int b = 0; b < equipped.size(); b++){
						if(equipped.get(b).getID().equalsIgnoreCase(items.get(a).getID())){
							room.addItem(items.get(a));
							items.remove(items.get(a));
							this.unEquip(item);
							return item + " has been unequipped and removed from inventory";
						} else if(equipped.get(b).getName().equalsIgnoreCase(items.get(a).getID())){
							room.addItem(items.get(a));
							items.remove(items.get(a));
							this.unEquip(item);
							return item + " has been unequipped and removed from inventory";
						}
					}
					room.addItem(items.get(a));
					items.remove(items.get(a));
					return item + " has been removed from inventory";
				}
			}
			return "Such item does not exist in your inventory";
		} catch(Exception e){
			return "Such item does not exist in your inventory";
		}
	}

	public String take(String item) {
		return takeHelper(item);
	}

	private String takeHelper(String item) {
		if (room.itemExists(item)) {
			items.add(room.getItem(item));
			room.removeItem(room.getItem(item));
			
			this.combinedGold();
			
			return item + " has been added to your inventory";
		} else {
			return "Such item does not exists in the room";
		}
	}

	private void combinedGold() {
		// TODO Auto-generated method stub
		int first = -1;
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int a=0; a<items.size(); a++){
			if(items.get(a) instanceof GoldPouch && first == -1){
				first = a;
			}else if( items.get(a) instanceof GoldPouch){
				((GoldPouch) items.get(first)).addGold(((GoldPouch) items.get(a)).getGold());
				
				list.add(a);
			}
		}
		
		for(int a=list.size()-1; a>-1; a--){
			items.remove(a);
		}
	}

	public String inventory() {
		if (items.isEmpty()) {
			return "you do not have any items in your inventory";
		} else {
			String inv = "Your inventory contains:\n";
			for (int a = 0; a < items.size(); a++) {
				inv += items.get(a).getID() + "\n";
			}
			return inv;
		}
	}

	@SuppressWarnings("rawtypes")
	public LinkedList getInventory() {
		return items;
	}

	public String kill(String object) {
		return "we need to work on this part later";
	}

	public String look(String object) {
		if(object == null || object.equals("")){
			return room.toString();
		}
		return room.look((Player)this, object);
	}

	public String getID() {
		// TODO Auto-generated method stub
		return id;
	}

	public String getStats() {
		return "HP : "+ hp +"/"+ maxHp +"\nATK: "+ attack +"\nDEF: "+ defense +"\nACC: "+ accuracy +"\nAGL: "+ agility;
	}

	public void echo(String string) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isItemEquipped(String location){
		if(location.compareToIgnoreCase("right hand") == 0){
			return rightHandEquip;
		} else if(location.compareToIgnoreCase("left hand") == 0){
			return leftHandEquip;
		} else if(location.compareToIgnoreCase("head") == 0){
			return headEquip;
		} else if(location.compareToIgnoreCase("chest") == 0){
			return chestEquip;
		} else if(location.compareToIgnoreCase("feet") == 0){
			return feetEquip;
		} else if(location.compareToIgnoreCase("leg") == 0){
			return legEquip;
		}
		return false;
	}
	
	public void  setEquipped(boolean result, String location){
		if(location.compareToIgnoreCase("right hand") == 0){
			rightHandEquip = result;
		} else if(location.compareToIgnoreCase("left hand") == 0){
			leftHandEquip = result;
		} else if(location.compareToIgnoreCase("head") == 0){
			headEquip = result;
		} else if(location.compareToIgnoreCase("chest") == 0){
			chestEquip = result;
		} else if(location.compareToIgnoreCase("feet") == 0){
			feetEquip = result;
		} else if(location.compareToIgnoreCase("leg") == 0){
			legEquip = result;
		} 
	}
	
	public String addEquipped(String item){
		for (int a=0; a<items.size(); a++) {
			if (items.get(a).getID().equalsIgnoreCase(item)) {
				equipped.add(items.get(a));
				return item + " has been equipped";
			}
		}
		return "Such Item does not exist";
		
	}
	
	public String removeEquipped(String item){
		for (int a=0; a<equipped.size(); a++) {
			if (equipped.get(a).getID().equalsIgnoreCase(item)) {
				equipped.remove(equipped.get(a));
				return item + " has been unequipped";
			}
		}
		return "Such Item does not exist";
	}
	
	public String getEquipped(){
		String result = "";
		for (int a=0; a<equipped.size(); a++) {
			result += equipped.get(a).getID() + "\n";
		}
		return result;
	}
	
	public String use(String item){
		String result = "";
		Item possible = useHelper(item);
		if(possible != null)
			result += possible.execute(this);
		else
			result = "You do not have such item in your inventory";
		return result;
	}
	
	private Item useHelper(String item){
		try{
		for (int a=0; a<items.size(); a++) {
			if (items.get(a).getID().equalsIgnoreCase(item)) {
				return items.get(a);
			} else if (items.get(a).getName().equalsIgnoreCase(item)) {
				return items.get(a);
			}
		}
		return null;
		} catch(Exception e){
			return null;
		}
	}
	
	public String unEquip(String item){
		String result = "";
		Item possible = unEquipHelper(item);
		if(possible != null)
			result += possible.unEquip(this);
		else
			result = "You did not equip such item";
		return result;
	}
	
	private Item unEquipHelper(String item){
		try{
			for (int a=0; a<equipped.size(); a++) {
				if (equipped.get(a).getID().equalsIgnoreCase(item)) {
					return equipped.get(a);
				} else if (equipped.get(a).getName().equalsIgnoreCase(item)) {
					return equipped.get(a);
				}
			}
			return null;
		} catch(Exception e){
			return null;
		}
	}

	public void clearInv() {
		items.clear();
	}

	public void resetStats() {
		hp = 100;
		vision = 0;
		defense = 0;
		attack = 1;
		agility = 20;
		accuracy = 100;
		inCombat = false;
		rightHandEquip = false;
		leftHandEquip = false;
		chestEquip = false;
		feetEquip = false;
		legEquip = false;
		headEquip = false;
	}
	
	public LinkedList<Item> getItems(){
		return items;
	}
}
