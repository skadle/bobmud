package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.items.ApolloBow;
import model.items.AttackBoots;
import model.items.BeginnerBow;
import model.items.BeginnerChest;
import model.items.BeginnerGun;
import model.items.BeginnerHelmet;
import model.items.BeginnerShield;
import model.items.BeginnerShoe;
import model.items.BeginnerSpear;
import model.items.BeginnerSword;
import model.items.CaptainJackGun;
import model.items.Corpse;
import model.items.DefenseBoots;
import model.items.FBsword;
import model.items.GladiatorHelmet;
import model.items.GodHelmet;
import model.items.GodSword;
import model.items.GoldPouch;
import model.items.IntermediateBow;
import model.items.IntermediateChest;
import model.items.IntermediateGun;
import model.items.IntermediateHelmet;
import model.items.IntermediateShield;
import model.items.IntermediateShoe;
import model.items.IntermediateSpear;
import model.items.IntermediateSword;
import model.items.Landuin;
import model.items.MajorHealthPotion;
import model.items.MajorManaPotion;
import model.items.MinorHealthPotion;
import model.items.MinorManaPotion;
import model.items.Stone;
import model.items.Torch;
import model.items.WarmongerChest;
import model.items.WoodStick;
import model.items.ZinXaoSpear;
import model.livings.CaptainOfPirate;
import model.livings.Chicken;
import model.livings.DavyJones;
import model.livings.FilthyPirate;
import model.livings.Kraken;
import model.livings.NavyCaptain;
import model.livings.NavySoldier;
import model.livings.Orc;
import model.livings.Pig;
import model.livings.Player;
import model.livings.Skeleton;
import model.livings.Zombie;
import server.MUDServer;
import zones.burningForest.BurningCave;
import zones.burningForest.BurningForestEntrance;
import zones.burningForest.DeeperBurningForest;
import zones.general.Jungle;
import zones.mountain.MountainPath;
import zones.sewers.Cellar;
import zones.sewers.ClockTower;
import zones.sewers.TowerBasement;
import zones.starterBeach.EdgeOfTheJungle;
import zones.starterBeach.StarterBeach;
import zones.starterBeach.StarterHill;
import zones.tortuga.Bar;
import zones.tortuga.Docks;
import zones.tortuga.Dutchman;
import zones.tortuga.Empire;
import zones.tortuga.Graveyard;
import zones.tortuga.Manor;
import zones.tortuga.Mission;
import zones.tortuga.Pier;
import zones.tortuga.TortugaBurnedBuilding;
import zones.tortuga.TortugaCenter;
import zones.tortuga.TortugaEast;
import zones.tortuga.TortugaNorth;
import zones.tortuga.TortugaSouth;
import zones.tortuga.TortugaWest;
import zones.tortuga.TradingPost;
import zones.tortuga.Warehouse;
import zones.wumpus.Dungeon;
import zones.wumpus.Dungeon2;
import zones.wumpus.Lost;
import zones.wumpus.Wumpus;
import zones.wumpus.WumpusCave;

public class Game {
	private List<Living> livings;
	private List<Room> rooms;
	private List<Combat> combat;

	public static void main(String args[]) {
		Game game = new Game();
	}

	public Game() {
		this.loadRooms();
		
//		this.testRooms();
		
		livings = new ArrayList<Living>();
		combat = new ArrayList<Combat>();
		

//		 Living liv1 = new Player("1", rooms.get(0));
//		 livings.add(liv1);
//		
//		 String ask = "";
//		 Scanner scan = new Scanner(System.in);
//		
//		
//		 System.out.println(parse("look", "1", null));
//		 do{
//		 ask = scan.nextLine();
//		
//		 System.out.println(parse(ask, "1", null));
//		 }while(!ask.equalsIgnoreCase("exit"));
	}

	/**
	 * parse(String str, String ID) --
	 * 
	 * this method takes the string passed to it and breaks it up so it can be
	 * used by the command method to pass the commands off to there appropriate
	 * methods in the game.
	 * 
	 * @param str
	 *            - the command that the player has entered
	 * @param ID
	 *            - the ID of the player in string form
	 * @return a string telling the user what has happened in the game
	 */
	public String parse(String str, String ID, MUDServer serv){
		if(str.length() > 0){
			ArrayList<String> list = new ArrayList<String>();
			Living playerCommand = getPlayer(ID);// gets the player from the ID
			Scanner scanner = new Scanner(str);
	
			String command = scanner.next();
			command = command.toLowerCase();
			String other = "";
			if (str.length() > command.length()) {
				try{
					other = str.substring(command.length() + 1);
				}catch(StringIndexOutOfBoundsException e){
					return "Please enter a valid command.";
				}
			}
	
			other = other.toLowerCase();
	
			if (playerCommand.getInCombat()) {
				if (command.equals("run")) {
					for (int a = 0; a < combat.size(); a++) {
						if (combat.get(a).hasPlayer(playerCommand)) {
							combat.get(a).endCombat();
						}
					}
				}
			}
	
			// start of move commands
			if (command.equals("go") || command.equals("move")) {
				list.add("move");
				command = scanner.next().toLowerCase();
				list.add(command);
				return command(list, playerCommand);
			}
			if (command.equals("north") || command.equals("n")) {
				list.add("move");
				list.add("north");
				return command(list, playerCommand);
			} else if (command.equals("south") || command.equals("s")) {
				list.add("move");
				list.add("south");
				return command(list, playerCommand);
			} else if (command.equals("east") || command.equals("e")) {
				list.add("move");
				list.add("east");
				return command(list, playerCommand);
			} else if (command.equals("west") || command.equals("w")) {
				list.add("move");
				list.add("west");
				return command(list, playerCommand);
			}
			// end of move commands
	
			else if (command.equals("take") || command.equals("get")) {// pick up
																		// command
				list.add("take");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (command.equals("kill") || command.equals("fight")) {// kill
																			// command
				// list.add("kill");
				// list.add(other);
				// return command(list, playerCommand);
				if (playerCommand.getRoom().livingExists(other)) {
					Combat cb = new Combat(serv, playerCommand, other);
					this.combat.add(cb);
					return "You are in combat";
				}
				return other + " is not in this room";
			}
	
			else if (command.equals("drop")) {// drop item command
				list.add("drop");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (command.equals("use") || command.equals("equip")) {// use item
																		// command
				list.add("use");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (command.equals("unequip")) {
				list.add("unequip");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (command.equals("look") || (command.equals("l"))) {// look
																		// command
				list.add("look");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (command.equals("inv") || command.equals("pack")
					|| command.equals("bag") || command.equals("inventory")) {// inventory
																				// command
				list.add("inv");
				return command(list, playerCommand);
			}
	
			else if (command.equals("stats") || command.equals("stat") || command.equals("score")) {// status
																			// command
				list.add("stats");
				return command(list, playerCommand);
			}
	
			else if (command.equals("help") || command.equals("commands") || command.equals("command")) {
				list.add("help");
				list.add(other);
				return command(list, playerCommand);
			}
			
			else if(command.equals("save")){
				list.add("save");
				return command(list, playerCommand);
			}
			
			else if(command.equals("_load")){
				list.add("load");
				list.add(other);
				return command(list, playerCommand);
			}
	
			else if (playerCommand.getRoom().hasCommand(command)) {// for commands
																	// that are
																	// specific to a
																	// room
				list.add(command);
				list.add(other);
				return playerCommand.getRoom().useCommand(list);
			}
			
			else if(playerCommand.isAdmin()){//addmin command
				if(command.equals("shutdown")){//shutDown command, shuts down the server
					serv.shutDown();
				}
			}
			return "Please enter a valid command.";
		}
		return "Please enter a valid command.";
	}

	/**
	 * getPlayer(String ID) --
	 * 
	 * returns a reference to the player that has the given user ID
	 * 
	 * @param String
	 *            -ID
	 * @return player- the player in the game that has the given ID
	 */
	private Living getPlayer(String ID) {
		for (int a = 0; a < livings.size(); a++) {
			if (livings.get(a).getID().equals(ID)) {
				return livings.get(a);
			}
		}
		return null;
	}

	private String command(ArrayList<String> list, Living player) {
		// TODO Auto-generated method stub

		if (list.get(0).equals("move")) {
			return player.moveRoom(list.get(1));
		} else if (list.get(0).equals("take")) {
			return player.take(list.get(1));
		} else if (list.get(0).equals("kill")) {
			return player.kill(list.get(1));
		} else if (list.get(0).equals("drop")) {
			return player.drop(list.get(1));
		} else if (list.get(0).equals("use")) {
			return player.use(list.get(1));
		} else if (list.get(0).equals("unequip")) {
			return player.unEquip(list.get(1));
		} else if (list.get(0).equals("look")) {
			return player.look(list.get(1));
		} else if (list.get(0).equals("inv")) {
			return player.inventory();
		} else if (list.get(0).equals("stats")) {
			return player.getStats();
		} else if (list.get(0).equals("help")) {
			return this.help();
		}else if(list.get(0).equals("save")){
			return ((Player) player).doSave();
		}else if(list.get(0).equals("load")){
			((Player) player).doLoad(list.get(1));
		}

		return "steve fucked up... this should never be printed";
	}

	private String help() {
		return  "go \"direction\"\n" +
				"move \"direction\"\n" +
				"\tYour player takes the exit from your current room in the direction\n" +
				"\tspecified. The directions north, south, east, and west can be \n" +
				"\tshortened by no typing the go or move before them also you can type\n" +
				"\tn, e, s, w\n" +
				"take \"item\" \n" +
				"get \"item\" \n" +
				"\tPicks up the specified item form the ground and \n" +
				"\tplaces it into your inventory. \n" +
				"kill \"creature or player \" \n" +
				"fight \"creature or player\" \n" +
				"\tYou enter into combat with the specified creature or player. \n" +
				"\trun\n" +
				"\t\tWhile in combat you can use the run\n" +
				"\t\tcommand to end the combat.\n" +
				"drop \"item\" \n" +
				"\tRemoves the specified item from your inventory and\n" +
				"\tplaces it into your current room\n" +
				"use \"item\" \n" +
				"\tUses the specified item\n " +
				"\t(use gold pouch will tell you how much gold you have) \n" +
				"equip \"item\" \n" +
				"\tEquips the specified item\n" +
				"Unequip \"item\"\n" +
				"\tUn-equips the specified item\n" +
				"look (item, creatures, exits, nouns in the rooms ) \n" +
				"L (item, creatures, exits, nouns in the rooms ) \n" +
				"\tWith no argument with look it will give you a general description\n" +
				"\tof the room you are in. With a arguments it will give you\n" +
				"\ta detailed description of that argument. \n" +
				"inv\n" +
				"inventory\n" +
				"pack\n" +
				"bag\n" +
				"\tWill tell you all of the items you have in you inventory. \n" +
				"stats\n" +
				"stat\n" +
				"score\n" +
				"\tWill show you your current stats. \n" +
				"save\n" +
				"\tThis command will save your current progress in the game.\n" +
				"help\n" +
				"commands\n" +
				"\tWill show you this message. \n" +
				"\n\n" +
				"There are hidden commands in rooms, to find them\n" +
				"use the look command and your imagination.\n";
	}

	public void addLiving(Living liv) {
		livings.add(liv);
	}

	public void addRoom(Room room) {
		rooms.add(room);
	}

	public Room getStartingRoom() {
		return rooms.get(0);
	}

	private void loadRooms() {
		
		Item mBow = new ApolloBow();
		Item aBoot	= new AttackBoots();
		Item bow = new BeginnerBow();
		Item chest = new BeginnerChest();
		Item gun = new BeginnerGun();
		Item helm = new BeginnerHelmet();
		Item shield = new BeginnerShield();
		Item shoe = new BeginnerShoe();
		Item spear = new BeginnerSpear();
		Item sword = new BeginnerSword();
		Item mGun = new CaptainJackGun();
		Item dBoot = new DefenseBoots();
		Item mSword = new FBsword();
		Item mHelm = new GladiatorHelmet();
		Item money = new GoldPouch();
		Item iBow = new IntermediateBow();
		Item iChest = new IntermediateChest();
		Item iGun = new IntermediateGun();
		Item iHelm = new IntermediateHelmet();
		Item iShield = new IntermediateShield();
		Item iShoe = new IntermediateShoe();
		Item iSpear = new IntermediateSpear();
		Item iSword = new IntermediateSword();
		Item mShield = new Landuin();
		Item minorHealth = new MinorHealthPotion();
		Item majorHealth = new MajorHealthPotion();
		Item MinorMana = new MinorManaPotion();
		Item MajorMana = new MajorManaPotion();
		Item stone = new Stone();
		Item torch = new Torch();
		Item mChest = new WarmongerChest();
		Item stick = new WoodStick();
		Item mSpear = new ZinXaoSpear();
//		Item corp = new Corpse();
		
		Living pCaptain = new CaptainOfPirate(null);
		Living chicken = new Chicken(null);
		Living davy = new DavyJones(null);
		Living pirate = new FilthyPirate(null);
		Living kraken = new Kraken(null);
		Living nCaptain = new NavyCaptain(null);
		Living navy = new NavySoldier(null);
		Living orc = new Orc(null);
		Living pig = new Pig(null);
		Living skel = new Skeleton(null);
		Living Zomb = new Zombie(null);
		
		rooms = new ArrayList<Room>();

		Room starterBeach = new StarterBeach("starterBeach");
		Room j1 = new Jungle("j1");
		Room j2 = new Jungle("j2");
		Room j3 = new Jungle("j3");
		Room j4 = new Jungle("j4");
		Room j5 = new Jungle("j5");
		Room j6 = new EdgeOfTheJungle("j6");
		Room starterHill = new StarterHill("starterHill");

		Room tortugaW = new TortugaWest("tw");
		Room tortugaS = new TortugaSouth("ts");
		Room tortugaC = new TortugaCenter("tc");
		Room tortugaE = new TortugaEast("te");
		Room tortugaN = new TortugaNorth("tn");
		Room mission = new Mission("mission");
		Room graveyard = new Graveyard("graveyard");
		Room manor = new Manor("manor");
		Room bar = new Bar("bar");
		Room shop = new TradingPost("shop");
		Room warehouse = new Warehouse("warehouse");
		Room docks1 = new Docks("d1");
		Room docks2 = new Docks("d2");
		Room docks3 = new Docks("d3");
		Room pier = new Pier("pier");
		Room dutchman = new Dutchman("dutchman");
		Room empire = new Empire("empire");
		Room tortugaBurn = new TortugaBurnedBuilding("tbb");

		Room clockTower = new ClockTower("clockTower");
		Room towerBasement = new TowerBasement("towerBasement");

		Room j11 = new Jungle("j11");
		Room j12 = new Jungle("j12");
		Room burning1 = new BurningForestEntrance("burning1");
		Room burning2 = new DeeperBurningForest("burning2");
		Room burningCave = new BurningCave("burningCave");

		Room j21 = new Jungle("j21");
		Room j22 = new Jungle("j22");
		Room j23 = new Jungle("j23");
		Room mountainPath = new MountainPath("mPath");

		Room cellar = new Cellar("cellar");

		starterBeach.addExit("jungle", j1);
		j1.addExit("beach", starterBeach);
		j1.addExit("northeast", j2);
		j2.addExit("east", j3);
		j2.addExit("southeast", j4);
		j3.addExit("west", j2);
		j3.addExit("south", j5);
		j4.addExit("northwest", j2);
		j4.addExit("east", j5);
		j5.addExit("north", j3);
		j5.addExit("northeast", j6);
		j6.addExit("southwest", j5);
		j6.addExit("east", starterHill);
		starterHill.addExit("west", j6);
		starterHill.addExit("down", tortugaS);

		tortugaC.addExit("south", tortugaS);
		tortugaC.addExit("east", tortugaE);
		tortugaC.addExit("north", tortugaN);
		tortugaN.addExit("south", tortugaC);
		tortugaN.addExit("north", j11);
		tortugaE.addExit("west", tortugaC);
		tortugaE.addExit("north", manor);
		tortugaE.addExit("south", bar);
		tortugaE.addExit("east", docks2);
		tortugaE.addExit("up", clockTower);
		clockTower.addExit("down", towerBasement);
		towerBasement.addExit("up", clockTower);
		towerBasement.addExit("east", cellar);
		cellar.addExit("west", towerBasement);
		shop.addExit("northwest", tortugaE);
		shop.addExit("east", warehouse);
		warehouse.addExit("west", shop);
		warehouse.addExit("east", docks3);
		tortugaE.addExit("southeast", shop);
		bar.addExit("north", tortugaE);
		tortugaS.addExit("north", tortugaC);
		tortugaS.addExit("west", tortugaW);
		tortugaS.addExit("south", mission);
		tortugaW.addExit("east", tortugaS);
		mission.addExit("north", tortugaS);
		mission.addExit("east", graveyard);
		graveyard.addExit("west", mission);
		manor.addExit("south", tortugaE);
		docks1.addExit("south", docks2);
		docks1.addExit("west", tortugaBurn);
		docks1.addExit("north", j21);
		docks1.addExit("east", pier);
		pier.addExit("west", docks1);
		docks2.addExit("north", docks1);
		docks2.addExit("south", docks3);
		docks2.addExit("west", tortugaE);
		docks2.addExit("east", dutchman);
		dutchman.addExit("west", docks2);
		docks3.addExit("west", warehouse);
		docks3.addExit("north", docks2);
		docks3.addExit("east", empire);
		empire.addExit("west", docks3);
		tortugaBurn.addExit("east", docks1);
		tortugaBurn.addExit("down", cellar);
		
		
		cellar.addExit("up", tortugaBurn);

		j11.addExit("south", tortugaN);
		j11.addExit("northwest", j12);
		j12.addExit("southeast", j11);
		j12.addExit("west", burning1);
		burning1.addExit("east", j12);
		burning1.addExit("deeper", burning2);
		burning2.addExit("out", burning1);
		burning2.addExit("cave", burningCave);
		burningCave.addExit("out", burning2);

		j21.addExit("south", docks1);
		j21.addExit("west", j22);
		j21.addExit("north", j23);
		j22.addExit("east", j21);
		j23.addExit("south", j21);
		j23.addExit("northeast", mountainPath);
		mountainPath.addExit("southwest", j23);

		// starterBeach j1 - j6 starterHill tortugaW tortugaS tortugaC tortugaE
		// tortugaN
		// mission graveyard manor bar shop warehouse docks1 -3 pier dutchman
		// empire tortugaBurn
		// j11 j12 burning1 burning2 burningCave j21 j22 j23 mountainPath cellar
		rooms.add(starterBeach);
		rooms.add(j1);
		rooms.add(j2);
		rooms.add(j3);
		rooms.add(j4);
		rooms.add(j5);
		rooms.add(j6);
		rooms.add(starterHill);
		rooms.add(tortugaW);
		rooms.add(tortugaS);
		rooms.add(tortugaC);
		rooms.add(tortugaE);
		rooms.add(tortugaN);
		rooms.add(mission);
		rooms.add(graveyard);
		rooms.add(manor);
		rooms.add(bar);
		rooms.add(shop);
		rooms.add(warehouse);
		rooms.add(docks1);
		rooms.add(docks2);
		rooms.add(docks3);
		rooms.add(pier);
		rooms.add(dutchman);
		rooms.add(empire);
		rooms.add(tortugaBurn);
		rooms.add(j11);
		rooms.add(j12);
		rooms.add(burning1);
		rooms.add(burning2);
		rooms.add(burningCave);
		rooms.add(j21);
		rooms.add(j22);
		rooms.add(j23);
		rooms.add(mountainPath);
		rooms.add(cellar);

		starterBeach.addItem(sword);
		starterBeach.addItem(stone);
		starterBeach.addItem(stone);
		starterBeach.addItem(stone);
		starterBeach.addItem(stick);
		starterBeach.addItem(stick);
		starterBeach.addLiving(kraken);
		starterBeach.addItem(minorHealth);
		
		j1.addLiving(chicken);
		j1.addLiving(chicken);
		j1.addLiving(pig);
//		j1.addItem(corp);
		
		j2.addItem(spear);
		j2.addLiving(pig);
		j2.addLiving(chicken);
		j2.addLiving(pig);
		j2.addLiving(chicken);
		j2.addLiving(chicken);
		j2.addItem(minorHealth);
		
		j3.addLiving(orc);
		j3.addLiving(pig);
		j3.addItem(stick);
		
		j4.addLiving(skel);
		j4.addLiving(chicken);
		j4.addLiving(chicken);
		j4.addItem(stone);
		j4.addItem(minorHealth);
		
		j5.addLiving(Zomb);
		j5.addItem(shoe);
		j5.addLiving(pig);
//		j5.addItem(corp);
		
		j6.addLiving(pirate);
		j6.addItem(chest);
		j6.addLiving(pig);
		j6.addItem(stone);
		j6.addItem(minorHealth);
		
		starterHill.addLiving(pirate);
		starterHill.addLiving(pirate);
		starterHill.addLiving(pCaptain);
		starterHill.addLiving(pig);
		starterHill.addLiving(chicken);
		starterHill.addItem(helm);
		starterHill.addItem(bow);
		starterHill.addItem(majorHealth);
		
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pCaptain);
		tortugaS.addLiving(pCaptain);
		tortugaS.addItem(stone);
		tortugaS.addItem(gun);
		tortugaS.addItem(minorHealth);
		
		mission.addItem(iSword);
		mission.addLiving(navy);
		mission.addLiving(navy);
		mission.addLiving(navy);
		mission.addLiving(skel);
		mission.addItem(minorHealth);
		mission.addItem(minorHealth);
		
		graveyard.addLiving(Zomb);
		graveyard.addLiving(Zomb);
		graveyard.addLiving(Zomb);
		graveyard.addLiving(skel);
		graveyard.addLiving(pig);
		graveyard.addLiving(orc);
		graveyard.addLiving(orc);
//		graveyard.addItem(corp);
		graveyard.addItem(minorHealth);
		
		tortugaW.addItem(stick);
		tortugaW.addItem(chest);
		tortugaW.addLiving(pirate);
		tortugaW.addLiving(navy);
		
		tortugaC.addLiving(navy);
		tortugaC.addLiving(nCaptain);
		tortugaC.addLiving(pirate);
		tortugaC.addLiving(pCaptain);
		tortugaC.addItem(minorHealth);
		tortugaC.addItem(MinorMana);
		
		tortugaN.addLiving(pig);
		tortugaN.addLiving(chicken);
		tortugaN.addLiving(pirate);
		tortugaN.addItem(stick);
		
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addItem(iGun);
		bar.addLiving(pCaptain);
		bar.addLiving(pCaptain);
		
		j11.addItem(iChest);
		j11.addLiving(orc);
		j11.addLiving(skel);
		
		burning1.addItem(iShield);
		
		burning2.addItem(stone);
		
		burningCave.addItem(stick);
		
		tortugaE.addLiving(pirate);
		tortugaE.addLiving(pirate);
		tortugaE.addLiving(navy);
		tortugaE.addLiving(navy);
		
		shop.addLiving(nCaptain);
		shop.addLiving(navy);
		shop.addLiving(navy);
		shop.addItem(iHelm);
		shop.addItem(iShoe);
		
		warehouse.addLiving(navy);
		warehouse.addLiving(pig);
		warehouse.addLiving(pig);
		
		docks3.addLiving(navy);
		docks3.addLiving(nCaptain);
		docks3.addItem(iSpear);
		
		empire.addLiving(nCaptain);
		empire.addLiving(nCaptain);
		empire.addLiving(nCaptain);
		empire.addItem(mSword);
		
		docks2.addLiving(pirate);
		docks2.addLiving(pirate);
		docks2.addLiving(pCaptain);
		docks2.addItem(iBow);
		
		dutchman.addLiving(pCaptain);
		dutchman.addLiving(pCaptain);
		dutchman.addLiving(pirate);
		dutchman.addLiving(davy);
		dutchman.addItem(mGun);
		
		docks1.addLiving(pirate);
		docks1.addLiving(orc);
//		docks1.addItem(corp);
		docks1.addLiving(chicken);
		
		pier.addLiving(kraken);
//		pier.addItem(corp);
//		pier.addItem(corp);
//		pier.addItem(corp);
//		pier.addItem(corp);
		
		tortugaBurn.addLiving(Zomb);
		tortugaBurn.addLiving(Zomb);
		tortugaBurn.addLiving(skel);
		
//		cellar.addItem(corp);
//		cellar.addItem(corp);
		cellar.addItem(majorHealth);
		cellar.addItem(majorHealth);
		cellar.addItem(majorHealth);
		
		clockTower.addItem(mHelm);
		clockTower.addItem(majorHealth);
		clockTower.addItem(majorHealth);
		clockTower.addItem(majorHealth);
		
		manor.addItem(mGun);
		manor.addItem(stone);
		
		j21.addLiving(pig);
		j21.addLiving(pig);
		j21.addLiving(pig);
		
		j22.addLiving(pig);
		j22.addLiving(chicken);

		/*
		rooms = new ArrayList<Room>();

		Room starterBeach = new StarterBeach("starterBeach");
		Room j1 = new Jungle("j1");
		Room j2 = new Jungle("j2");
		Room j3 = new Jungle("j3");
		Room j4 = new Jungle("j4");
		Room j5 = new Jungle("j5");
		Room j6 = new EdgeOfTheJungle("j6");
		Room starterHill = new StarterHill("starterHill");

		Room tortugaW = new TortugaWest("tw");
		Room tortugaS = new TortugaSouth("ts");
		Room tortugaC = new TortugaCenter("tc");
		Room tortugaE = new TortugaEast("te");
		Room tortugaN = new TortugaNorth("tn");
		Room mission = new Mission("mission");
		Room graveyard = new Graveyard("graveyard");
		Room manor = new Manor("manor");
		Room bar = new Bar("bar");
		Room shop = new TradingPost("shop");
		Room warehouse = new Warehouse("warehouse");
		Room docks1 = new Docks("d1");
		Room docks2 = new Docks("d2");
		Room docks3 = new Docks("d3");
		Room pier = new Pier("pier");
		Room dutchman = new Dutchman("dutchman");
		Room empire = new Empire("empire");
		Room tortugaBurn = new TortugaBurnedBuilding("tbb");

		Room clockTower = new ClockTower("clockTower");
		Room towerBasement = new TowerBasement("towerBasement");

		Room j11 = new Jungle("j11");
		Room j12 = new Jungle("j12");
		Room burning1 = new BurningForestEntrance("burning1");
		Room burning2 = new DeeperBurningForest("burning2");
		Room burningCave = new BurningCave("burningCave");

		Room j21 = new Jungle("j21");
		Room j22 = new Jungle("j22");
		Room j23 = new Jungle("j23");
		Room mountainPath = new MountainPath("mPath");

		Room cellar = new Cellar("cellar");

		starterBeach.addExit("jungle", j1);
		j1.addExit("beach", starterBeach);
		j1.addExit("northeast", j2);
		j2.addExit("east", j3);
		j2.addExit("southeast", j4);
		j3.addExit("west", j2);
		j3.addExit("south", j5);
		j4.addExit("northwest", j2);
		j4.addExit("east", j5);
		j5.addExit("north", j3);
		j5.addExit("northeast", j6);
		j6.addExit("southwest", j5);
		j6.addExit("east", starterHill);
		starterHill.addExit("west", j6);
		starterHill.addExit("down", tortugaS);

		tortugaC.addExit("south", tortugaS);
		tortugaC.addExit("east", tortugaE);
		tortugaC.addExit("north", tortugaN);
		tortugaN.addExit("south", tortugaC);
		tortugaN.addExit("north", j11);
		tortugaE.addExit("west", tortugaC);
		tortugaE.addExit("north", manor);
		tortugaE.addExit("south", bar);
		tortugaE.addExit("east", docks2);
		tortugaE.addExit("up", clockTower);
		clockTower.addExit("down", towerBasement);
		towerBasement.addExit("up", clockTower);
		towerBasement.addExit("east", cellar);
		cellar.addExit("west", towerBasement);
		shop.addExit("northwest", tortugaE);
		shop.addExit("east", warehouse);
		warehouse.addExit("west", shop);
		warehouse.addExit("east", docks3);
		tortugaE.addExit("southeast", shop);
		bar.addExit("north", tortugaE);
		tortugaS.addExit("north", tortugaC);
		tortugaS.addExit("west", tortugaW);
		tortugaS.addExit("south", mission);
		tortugaW.addExit("east", tortugaS);
		mission.addExit("north", tortugaS);
		mission.addExit("east", graveyard);
		graveyard.addExit("west", mission);
		manor.addExit("south", tortugaE);
		docks1.addExit("south", docks2);
		docks1.addExit("west", tortugaBurn);
		docks1.addExit("north", j21);
		docks1.addExit("east", pier);
		pier.addExit("west", docks1);
		docks2.addExit("north", docks1);
		docks2.addExit("south", docks3);
		docks2.addExit("west", tortugaE);
		docks2.addExit("east", dutchman);
		dutchman.addExit("west", docks2);
		docks3.addExit("west", warehouse);
		docks3.addExit("north", docks2);
		docks3.addExit("east", empire);
		empire.addExit("west", docks3);
		tortugaBurn.addExit("east", docks1);
		tortugaBurn.addExit("down", cellar);
		
		
		cellar.addExit("up", tortugaBurn);

		j11.addExit("south", tortugaN);
		j11.addExit("northwest", j12);
		j12.addExit("southeast", j11);
		j12.addExit("west", burning1);
		burning1.addExit("east", j12);
		burning1.addExit("deeper", burning2);
		burning2.addExit("out", burning1);
		burning2.addExit("cave", burningCave);
		burningCave.addExit("out", burning2);

		j21.addExit("south", docks1);
		j21.addExit("west", j22);
		j21.addExit("north", j23);
		j22.addExit("east", j21);
		j23.addExit("south", j21);
		j23.addExit("northeast", mountainPath);
		mountainPath.addExit("southwest", j23);
		*/
		//wumpus zone
		Room d2 = new Dungeon2();
		Room dun = new Dungeon(d2);
		Room l1 = new Lost();
		Room l2 = new Lost();
		Room l3 = new Lost();
		Room l4 = new Lost();
		Room pit = new WumpusCave();
		
		Living wumpus = new Wumpus(pit);
		pit.addLiving(wumpus);
		
		bar.addExit("down", dun);
		dun.addExit("up", bar);
		
		d2.addExit("door", dun);
		d2.addExit("maze", l1);
		
		l1.addExit("north", l3);
		l1.addExit("east", l2);
		l1.addExit("south", l2);
		
		l2.addExit("south", l3);
		l2.addExit("north", l4);
		l2.addExit("west", l3);
		
		l3.addExit("south", l2);
		l3.addExit("west", l4);
		l3.addExit("east", l4);
		
		l4.addExit("north", l1);
		l4.addExit("east", pit);
		l4.addExit("west", l1);
		
		pit.addExit("cellar", cellar);
		
		
		
		

		// starterBeach j1 - j6 starterHill tortugaW tortugaS tortugaC tortugaE
		// tortugaN
		// mission graveyard manor bar shop warehouse docks1 -3 pier dutchman
		// empire tortugaBurn
		// j11 j12 burning1 burning2 burningCave j21 j22 j23 mountainPath cellar
		rooms.add(starterBeach);
		rooms.add(j1);
		rooms.add(j2);
		rooms.add(j3);
		rooms.add(j4);
		rooms.add(j5);
		rooms.add(j6);
		rooms.add(starterHill);
		rooms.add(tortugaW);
		rooms.add(tortugaS);
		rooms.add(tortugaC);
		rooms.add(tortugaE);
		rooms.add(tortugaN);
		rooms.add(towerBasement);
		rooms.add(mission);
		rooms.add(graveyard);
		rooms.add(manor);
		rooms.add(bar);
		rooms.add(shop);
		rooms.add(warehouse);
		rooms.add(docks1);
		rooms.add(docks2);
		rooms.add(docks3);
		rooms.add(pier);
		rooms.add(dutchman);
		rooms.add(empire);
		rooms.add(tortugaBurn);
		rooms.add(j11);
		rooms.add(j12);
		rooms.add(burning1);
		rooms.add(burning2);
		rooms.add(burningCave);
		rooms.add(j21);
		rooms.add(j22);
		rooms.add(j23);
		rooms.add(mountainPath);
		rooms.add(cellar);
		rooms.add(d2);
		rooms.add(dun);
		rooms.add(l1);
		rooms.add(l2);
		rooms.add(l3);
		rooms.add(l4);
		rooms.add(pit);
		

	}
	
	private void testRooms(){
		rooms = new ArrayList<Room>();
		
		Room j1 = new Jungle("j1");
		Room j2 = new Jungle("j2");
		Room j3 = new Jungle("j3");
		Room j4 = new Jungle("j4");
		Room j5 = new Jungle("j5");
		
		j1.addExit("east", j2);
		j1.addExit("south", j3);
		j2.addExit("west", j1);
		j2.addExit("south", j4);
		
		j3.addExit("north", j1);
		j3.addExit("east", j4);
		j3.addExit("south", j5);
		
		j4.addExit("north", j2);
		j4.addExit("west", j3);
		
		j5.addExit("north", j4);
		
		Living chicken = new Chicken(j1);
		j1.addLiving(chicken);
		j1.addLiving(chicken);
		j1.addLiving(chicken);
		j1.addLiving(chicken);
		
		Living krak = new Kraken(j2);
		Living devy = new DavyJones(j2);
		
		j2.addLiving(krak);
		j2.addLiving(devy);
		
		Item Bgun = new BeginnerGun();
		Item godSword = new GodSword();
		Item godHelmet = new GodHelmet();
		
		j1.addItem(Bgun);
		j1.addItem(godHelmet);
		
		j4.addItem(godSword);
		
		rooms.add(j1);
		rooms.add(j2);
		rooms.add(j3);
		rooms.add(j4);
		rooms.add(j5);
		
		
	}

	public void respan(Living player) {
		player.setRoom(rooms.get(0));
		player.clearInv();
		player.resetStats();
	}
	
	public Room getRoom(String roomID){
		for(int a=0; a<rooms.size(); a++){
			if(rooms.get(a).getID().equalsIgnoreCase(roomID)){
				return rooms.get(a);
			}
		}
		return null;
	}
	
	public ArrayList<String> getPlayerIDRoom(String id){
		ArrayList<String> list = new ArrayList<String>();
		Living temp = null;
		
		for(int a=0 ;a< livings.size(); a++){
			if(livings.get(a).getID().equalsIgnoreCase(id)){
				temp = livings.get(a);
			}
		}
		
		for(int a=0; a<livings.size(); a++){
			if(livings instanceof Player){
				if(livings.get(a).getRoom() == temp.getRoom()){
					list.add(livings.get(a).getID());
				}
			}
		}
		
		return list;
	}
}
