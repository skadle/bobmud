package model;

import java.io.Serializable;
import java.util.LinkedList;

import model.livings.Player;

public class Save implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name, shortName, desc, roomID;
	int hp;
	LinkedList<String> itemIDs = new LinkedList<String>();

	public Save(Player player) {
		name = player.getName();
		shortName = player.getName();
		desc = player.getDesc();
		hp = player.getHP();
		roomID = player.getRoom().getID();
		itemIDs.clear();
		saveItems(player.getItems());
	}

	private void saveItems(LinkedList<Item> items) {
		for (int i = 0; i < items.size(); i++) {
			itemIDs.add(items.get(i).getID());
		}
		itemIDs.add(null);
		// TODO Auto-generated method stub

	}

	public String getItemIDAt(int i) {
		return itemIDs.get(i);
	}

	public String getRoomID() {
		return roomID;
	}

	public String getName() {
		return name;
	}

	public String getShortName() {
		return shortName;
	}

	public String getDesc() {
		return desc;
	}

	public int getHP() {
		return hp;
	}
}
