package model;

import game.AllItems;

import java.util.Vector;


public abstract class Item {

	protected Vector<String> tags = new Vector<String>();
	protected String name;
	protected String desc;
	protected String id;
	protected int weight;
	protected int attack;
	protected int accuracy;
	protected int defense;
	protected int agility;
	protected String equipMessage;
	protected String equipFail;
	protected String unequipMessage;

	// private String [] tag =

	public Item() {
//		id = "DEFAULT_ID";
		setDefaults();
	}

	public Item(String id) {
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		AllItems.add(this);
		name = "A thing";
		desc = "    This is a thing. There are many like it, but this thing is yours. It's heavy.";
		weight = 0;
		attack = 0;
		accuracy = 0;
		defense = 0;
		agility = 0;
		equipMessage = "You have equipped " + name + ".";
		equipFail = "Default equip failure message from ITEM";
		unequipMessage = "You have uneqiupped " + name + ".";

	}

	public void setName(String name) {
		this.name = name;
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public boolean hasTag(String str) {
		return tags.contains(str);
	}

	public void chatter() {
		// TODO Auto-generated method stub
	}

	public abstract String execute(Living living);

	public String unEquip(Living living) {
		// TODO Auto-generated method stub
		return null;
	}
}
