package model.livings;

import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;

public class Chicken extends Creature{

	public Chicken(Room room) {
		super("Chicken", room);
		desc = "a regular chicken";
		name = "chicken";
		hp = 2;
		maxHp=hp;
		attack = 1;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(4);
		Item goldPouch = new GoldPouch(gold);
		items.add(goldPouch);
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
