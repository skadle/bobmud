package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GodArmor;
import model.items.GodShoe;
import model.items.GodSword;
import model.items.GodTrident;
import model.items.GoldPouch;

public class Kraken extends Creature{
	
	private List<Item> root = new LinkedList<Item>();
	private Item temp = new GodArmor();
	private Item temp2 = new GodSword();
	private Item temp3 = new GodTrident();
	private Item temp4 = new GodShoe();

	public Kraken( Room room) {
		super("Kraken", room);
		desc = "a lengendary creature of ocean";
		name = "kraken";
		hp = 10000;
		maxHp=hp;
		attack = 100;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(10000);
		Item goldPouch = new GoldPouch(gold + 1000);
		root.add(temp);
		root.add(temp2);
		root.add(temp3);
		root.add(temp4);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

}
