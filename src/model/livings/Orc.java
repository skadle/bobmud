package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;
import model.items.IntermediateBow;
import model.items.IntermediateSpear;
import model.items.IntermediateSword;

public class Orc extends Creature{

	private List<Item> root = new LinkedList<Item>();
	private Item temp = new IntermediateSword();
	private Item temp2 = new IntermediateBow();
	private Item temp3 = new IntermediateSpear();

	public Orc(Room room) {
		super("Orc", room);
		desc = "weird creature called orc does not look like human at all";
		hp = 5;
		maxHp=hp;
		attack = 1;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(8);
		Item goldPouch = new GoldPouch(gold);
		root.add(temp);
		root.add(temp2);
		root.add(temp3);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		
	}
}
