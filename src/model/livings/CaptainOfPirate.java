package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.AttackBoots;
import model.items.CaptainJackGun;
import model.items.FBsword;
import model.items.GoldPouch;

public class CaptainOfPirate extends Creature{

	private List<Item> root = new LinkedList<Item>();
	private Item temp = new CaptainJackGun();
	private Item temp2 = new FBsword();
	private Item temp3 = new AttackBoots();

	public CaptainOfPirate(Room room) {
		super("Captain of Pirate", room);
		desc = "a captain of a pirates";
		name = "captain of pirate";
		hp = 70;
		maxHp=hp;
		attack = 20;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(100);
		Item goldPouch = new GoldPouch(gold + 50);
		root.add(temp);
		root.add(temp2);
		root.add(temp3);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
	}
}
