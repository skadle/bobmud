package model.livings;

import java.util.LinkedList;

import model.Item;
import model.items.BeginnerBow;
import model.items.BeginnerChest;
import model.items.BeginnerGun;
import model.items.BeginnerHelmet;
import model.items.BeginnerShield;
import model.items.BeginnerShoe;
import model.items.BeginnerSpear;

public class Vendor extends Creature{
	
	private Item bow = new BeginnerBow("bow");
	private Item chest = new BeginnerChest("chest");
	private Item helmet = new BeginnerHelmet("helm");
	private Item gun = new BeginnerGun("gun");
	private Item shield = new BeginnerShield("shield");
	private Item shoe = new BeginnerShoe("shoe");
	private Item spear = new BeginnerSpear("spear");
	

	protected LinkedList<Item> item = new LinkedList<Item>();
	
	public Vendor(String id, model.Room room) {
		super(id, room);
		item.add(bow);
		item.add(chest);
		item.add(helmet);
		item.add(gun);
		item.add(shield);
		item.add(shoe);
		item.add(spear);
	}
	
	public String listOfItems(){
		String inv = "I have :\n";
		for(int a=0; a<item.size(); a++){
			inv += item.get(a).getID() + "\n";
		}
		return inv;
	}
	
	public String sellToPlayer(Player player, Item item){
		room.addItem(item);
		//player.add(item.getID());
		return "You have purchased " + item + " from the vendor";
	}

	public String buyFromPlayer(Player player, String item){
		//player.remove(item);
		return "You have sold " + item + " to the vendor";
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
