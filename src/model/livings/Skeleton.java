package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;
import model.items.IntermediateShield;
import model.items.IntermediateShoe;

public class Skeleton extends Creature{

	private List<Item> root = new LinkedList<Item>();
	private Item temp = new IntermediateShield();
	private Item temp2 = new IntermediateShoe();

	public Skeleton(Room room) {
		super("Skeleton", room);
		desc ="a craeture that only have bones";
		name = "skeleton";
		hp = 10;
		maxHp=hp;
		attack = 3;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(7);
		Item goldPouch = new GoldPouch(gold + 2);
		root.add(temp);
		root.add(temp2);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

}
