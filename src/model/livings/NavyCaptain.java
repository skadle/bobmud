package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.DefenseBoots;
import model.items.GoldPouch;
import model.items.Landuin;
import model.items.WarmongerChest;

public class NavyCaptain extends Creature{
	
	private List<Item> root = new LinkedList<Item>();
	private Item temp = new Landuin();
	private Item temp2 = new WarmongerChest();
	private Item temp3 = new DefenseBoots();

	public NavyCaptain(Room room) {
		super("Navy Captain", room);
		desc = "a captain of navy";
		name = "captain of navy";
		hp = 75;
		maxHp=hp;
		attack = 19;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(100);
		Item goldPouch = new GoldPouch(gold + 200);
		root.add(temp);
		root.add(temp2);
		root.add(temp3);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
