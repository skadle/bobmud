package model.livings;

import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;

public class Pig extends Creature{
	
	public Pig(Room room) {
		super("Pig", room);
		desc = "a fat pig";
		name ="pig";
		hp = 3;
		maxHp=hp;
		attack = 1;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(4);
		Item goldPouch = new GoldPouch(gold + 1);
		items.add(goldPouch);
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
