package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;
import model.items.IntermediateChest;
import model.items.IntermediateHelmet;

public class Zombie extends Creature{
	
	private List<Item> root = new LinkedList<Item>();
	private Item temp = new IntermediateChest();
	private Item temp2 = new IntermediateHelmet();

	public Zombie(Room room) {
		super("Zombie", room);
		desc = "a creature that is not dead but not alive either...";
		name = "zombie";
		hp = 25;
		maxHp=hp;
		attack = 4;
		root.add(temp);
		root.add(temp2);
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(7);
		Item goldPouch = new GoldPouch(gold + 3);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
