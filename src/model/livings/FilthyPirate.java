package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;
import model.items.IntermediateChest;
import model.items.IntermediateGun;

public class FilthyPirate extends Creature{

	private List<Item> root = new LinkedList<Item>();
	private Item temp = new IntermediateGun();
	private Item temp2 = new IntermediateChest();
	
	public FilthyPirate(Room room) {
		super("Filthy Pirate", room);
		desc = "a filthy pirate";
		name = "filthy pirate";
		hp = 5;
		maxHp=hp;
		attack = 2;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(20);
		Item goldPouch = new GoldPouch(gold + 10);
		root.add(temp);
		root.add(temp2);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
