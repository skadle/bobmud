package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GladiatorHelmet;
import model.items.GoldPouch;
import model.items.ZinXaoSpear;

public class NavySoldier extends Creature{
	
	private List<Item> root = new LinkedList<Item>();
	private Item temp = new ZinXaoSpear();
	private Item temp2 = new GladiatorHelmet();

	public NavySoldier(Room room) {
		super("Navy Soldier", room);
		desc = "a regular navy soldier";
		name = "navy soldier";
		hp = 10;
		maxHp=hp;
		attack = 3;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(50);
		Item goldPouch = new GoldPouch(gold + 20);
		root.add(temp);
		root.add(temp2);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
