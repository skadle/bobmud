package model.livings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Item;
import model.Living;
import model.Save;
import model.items.ApolloBow;
import model.items.AttackBoots;
import model.items.BeginnerBow;
import model.items.BeginnerChest;
import model.items.BeginnerGun;
import model.items.BeginnerHelmet;
import model.items.BeginnerShield;
import model.items.BeginnerShoe;
import model.items.BeginnerSpear;
import model.items.BeginnerSword;
import model.items.CaptainJackGun;
import model.items.DefenseBoots;
import model.items.FBsword;
import model.items.GladiatorHelmet;
import model.items.GodArmor;
import model.items.GodHelmet;
import model.items.GodShield;
import model.items.GodShoe;
import model.items.GodSword;
import model.items.GodTrident;
import model.items.GoldPouch;
import model.items.Landuin;
import model.items.MajorHealthPotion;
import model.items.MinorHealthPotion;
import model.items.Stone;
import model.items.WarmongerChest;
import model.items.WoodStick;
import model.items.ZinXaoSpear;
import zones.starterBeach.BatteredKnife;
 
 public class Player extends Living {
 
 	public Player(String id, model.Room room) {
 		super(id, room);
		commands.add("save");

		
 		Item gold = new GoldPouch();
 		items.add(gold);
 	}
 

//	public String doSave(Player player) {
//		Save save = new Save(player);
//		return ;
//	}
	public String doSave() {
		Save save = new Save(this);
 		try {
 			FileOutputStream fileOut = new FileOutputStream(this.getName()
 					+ ".ser");
 			ObjectOutputStream out = new ObjectOutputStream(fileOut);
 			out.writeObject(save);
 			out.close();
 			fileOut.close();
 			System.out.println(this.getName() + " saved. (from Player.java)");
 
 		} catch (FileNotFoundException e) {
 			e.printStackTrace();
 		} catch (IOException e) {
 			e.printStackTrace();
 		}
		
		return "The game has been saved.";
 	}
 
 	public void doLoad(String string) {
 		Save save = null;
 
 		try {
 			FileInputStream fileIn = new FileInputStream(string + ".ser");
 			ObjectInputStream in = new ObjectInputStream(fileIn);
 
 			save = (Save) in.readObject();
 			in.close();
 			fileIn.close();
 			this.setName(save.getName());
 			this.setDesc(save.getDesc());
 			this.setShortName(save.getShortName());
 			this.setHP(save.getHP());
			this.items.clear();
			int i = 0;
			Item temp = new ApolloBow();
			while (save.getItemIDAt(i) != null) {

				switch (save.getItemIDAt(i)) {
				case "bow":
					temp = new ApolloBow();
					break;
				case "boots":
					temp = new AttackBoots();
					break;
				case "shortbow":
					temp = new BeginnerBow();
					break;
				case "plate":
					temp = new BeginnerChest();
					break;
				case "blunderbuss":
					temp = new BeginnerGun();
					break;
				case "helmet":
					temp = new BeginnerHelmet();
					break;
				case "shield":
					temp = new BeginnerShield();
					break;
				case "shoes":
					temp = new BeginnerShoe();
					break;
				case "spear":
					temp = new BeginnerSpear();
					break;
				case "sword":
					temp = new BeginnerSword();
					break;
				case "repeater":
					temp = new CaptainJackGun();
					break;
				case "sabatons":
					temp = new DefenseBoots();
					break;
				case "FBSword":
					temp = new FBsword();
					break;
				case "armet":
					temp = new GladiatorHelmet();
					break;
				case "GodArmor":
					temp = new GodArmor();
					break;
				case "GodHelmet":
					temp = new GodHelmet();
					break;
				case "GodShield":
					temp = new GodShield();
					break;
				case "GodShoes":
					temp = new GodShoe();
					break;
				case "GodSword":
					temp = new GodSword();
					break;
				case "GodTrident":
					temp = new GodTrident();
					break;
				case "Landuin":
					temp = new Landuin();
					break;
				case "Major Health Potion":
					temp = new MajorHealthPotion();
					break;
				case "Minor Health Potion":
					temp = new MinorHealthPotion();
					break;
				case "stone":
					temp = new Stone();
					break;
				case "chest":
					temp = new WarmongerChest();
					break;
				case "stick":
					temp = new WoodStick();
					break;
				case "ZinXaoSpear":
					temp = new ZinXaoSpear();
					break;
				case "knife":
					temp = new BatteredKnife();
					break;
				}
				items.add(temp);

			}
 			// TODO: the following won't work of course, have to work this out
 			// this.setRoom(save.getRoomID());
 			System.out.println(this.getName() + " loaded. (from Player.java)");
 		} catch(Exception e){
 			
 		}
 	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}