package model.livings;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GodArmor;
import model.items.GodSword;
import model.items.GoldPouch;

public class DavyJones extends Creature{
	
	private List<Item> root = new LinkedList<Item>();
	private Item temp = new GodSword();
	private Item temp2 = new GodArmor();

	public DavyJones(Room room) {
		super("Davy Jones", room);
		desc = "fearsome and legendary pirate Davy Jones";
		name = "Davy Jones";
		hp = 150;
		maxHp=hp;
		attack = 34;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(100);
		Item goldPouch = new GoldPouch(gold + 200);
		root.add(temp);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(temp2);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
	}
	
	public int getHp(){
		return hp;
	}
	
	public int getAttack(){
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}
}
