package model;

import game.AllRooms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import model.livings.Player;

public abstract class Room {
	protected HashMap<String, Room> exits = new HashMap<String, Room>();
	protected HashMap<String, String> nouns = new HashMap<String, String>();
	protected LinkedList<Living> livings = new LinkedList<Living>();
	protected LinkedList<Item> items = new LinkedList<Item>();
	protected ArrayList<String> commands = new ArrayList<String>();
	protected String name;
	protected String desc;
	protected int light;
	protected String id;

	public Room(String id) {
		this.id = id;
		this.name = id;
		setDefaults();
	}

	protected void setDefaults() {
		// AllRooms.add(this);
		name = "A room";
		desc = "    This is a room. You are in this room. \nThere are many like it, but this one is yours.";
		light = 1;
		nouns.put("room", desc);
		nouns.put("here", desc);
	}

	// look returns a string with desc, exits, livings, and items.
	public String toString() {
		String ret = "";
		ret += desc;
		// print all the exits
		if (exits.size() > 0) {
			ret += "\n    The only obvious exit";
			if (exits.size() == 1) {
				ret += " is " + exits.keySet().toArray()[0] + ".";
			} else {
				ret += "s are ";
				for (int i = 0; i < exits.size(); i++) {
					ret += exits.keySet().toArray()[i];
					if (i < exits.size() - 1)
						ret += " and ";
					else
						ret += ".";
				}
			}
			// if no exits>>
		} else
			ret += "\n    There are no obvious exits.";
		// adds all livings
		if (livings.size() > 0)
			for (int i = 0; i < livings.size(); i++) {
				ret += "\n " + livings.get(i).getName();
			}
		// adds all items
		if (items.size() > 0)
			for (int i = 0; i < items.size(); i++) {
				ret += "\n " + items.get(i).getID();
			}
		// add a newline for formatting
		ret += "\n";
		return ret;
	}

	// This version of toString does not return the current player in
	public String toString(Living living) {
		String ret = "";
		ret += desc;
		// print all the exits
		if (exits.size() > 0) {
			ret += "\n    The only obvious exit";
			if (exits.size() == 1) {
				ret += " is " + exits.keySet().toArray()[0] + ".";
			} else {
				ret += "s are ";
				for (int i = 0; i < exits.size(); i++) {
					ret += exits.keySet().toArray()[i];
					if (i < exits.size() - 1)
						ret += " and ";
					else
						ret += ".";
				}
			}
			// if no exits>>
		} else
			ret += "\n    There are no obvious exits.";
		// adds all livings
		if (livings.size() > 0)
			for (int i = 0; i < livings.size(); i++) {
				if (livings.get(i) != living)
					ret += "\n " + livings.get(i).getName();
			}
		// adds all items
		if (items.size() > 0)
			for (int i = 0; i < items.size(); i++) {
				ret += "\n " + items.get(i).getID();
			}
		// add a newline for formatting
		ret += "\n";
		return ret;
	}

	protected String look(Player player, String string) {

		// check items in the player's inventory
		for (int i = 0; i < player.getInventory().size(); i++) {
			Item item = (Item) player.getInventory().get(i);
			if (item.hasTag(string)) {
				return item.getDesc();
			}
		}

		// check livings in the room
		for (int i = 0; i < this.livings.size(); i++) {
			if (livings.get(i).getID().equalsIgnoreCase(string)) {
				String str = livings.get(i).getDesc();
				
				return str;
			}
		}

		// check items on the floor
		for (int i = 0; i < this.items.size(); i++) {
			if (items.get(i).getID().equalsIgnoreCase(string)) {
				return livings.get(i).getDesc();
			}
		}

		// check exit names
		if (exits.containsKey(string)) {
			return exits.get(string).getDesc();
		}

		// check described nouns
		if (nouns.containsKey(string)) {
			return nouns.get(string);
		}

		// return fail message
		return "You don't see that here.";
	}

	public Room move(String direction, Living living) {
		// TODO: move to a different room
		livings.remove(living);
		exits.get(direction).addLiving(living);
		return exits.get(direction);
	}

	public boolean possibleExit(String direction) {
		return exits.containsKey(direction);
	}

	public void addExit(String direction, Room exitRoom) {
		exits.put(direction, exitRoom);
	}

	public void addItem(Item item) {
		items.add(item);
	}

	public void removeItem(Item item) {
		items.remove(item);
	}

	/**
	 * itemExists(String string) --
	 * 
	 * checks to see if the string passed as a parameter is the id of any item
	 * contained in the room. returns true if the item is in the room and false
	 * if it is not in the room.
	 * 
	 * @param string
	 *            - the id of an item to find
	 * @return true if the item is contained in the room, false otherwise
	 */
	public boolean itemExists(String string) {
		for (int a = 0; a < items.size(); a++) {
			if (items.get(a).getID().equalsIgnoreCase(string)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * getItem(String itemName) --
	 * 
	 * this method searches the items in room and returns a reference to the
	 * item with the id that is equal to the argument string itemName. this
	 * method will return a reference to the item if it is in room otherwise it
	 * returns null.
	 * 
	 * @param itemName
	 *            - a string that is the id of an item that is being searched
	 *            for
	 * @return Item - returns a reference of the item that has the same id as
	 *         the parameter
	 */
	public Item getItem(String itemName) {
		for (int a = 0; a < items.size(); a++) {
			if (items.get(a).getID().equalsIgnoreCase(itemName)) {
				return items.get(a);// returns the first instance of the item
									// searched for
			}
		}
		return null;// if the item is not in the list then return nul
	}

	public void addLiving(Living living) {
		livings.add(living);
	}

	public void removeLiving(Living living) {
		livings.remove(living);
	}

	public boolean livingExists(String string) {
		for(int a=0; a<livings.size(); a++){
			if(livings.get(a).getID().equalsIgnoreCase(string)){
				return true;
			}
		}
		return false;
	}

	public Living getLiving(String string) {
		for(int a=0; a<livings.size(); a++){
			if(livings.get(a).getID().equalsIgnoreCase(string)){
				return livings.get(a);
			}
		}
		return null;
	}
	
	public ArrayList<Living> getAllPlayers(){
		ArrayList<Living> list  = new ArrayList<Living>();
		
		for(int a=0 ;a< livings.size(); a++){
			if (livings.get(a) instanceof Player){
				list.add(livings.get(a));
			}
		}
		
		return list;
	}
	
	public Set<String> getDirections(){
		return exits.keySet();
	}

	public String getID() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setLight(int light) {
		this.light = light;
	}

	// the abstract chatter method is for things that the room does
	public abstract void chatter();

	/**
	 * hasCommand(String command) --
	 * 
	 * this method checks the commands list in room to see if it contains the
	 * command passed to it in its arguments, if so it returns true if not it
	 * returns false.
	 * 
	 * @param command
	 *            - a string of a command to check for in this room
	 * @return boolean - true if the command exists in this room, otherwise
	 *         fales
	 */
	public boolean hasCommand(String command) {
		return commands.contains(command);
	}

	public abstract String useCommand(ArrayList<String> command);


}
