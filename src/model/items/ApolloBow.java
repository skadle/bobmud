package model.items;

public class ApolloBow extends Model2HWeapon {

	public ApolloBow() {
		super();
		id = "bow";
		setDefaults();
	}

	public ApolloBow(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "Legendary Apollo's Bow that can be looted from monsters";
		name = "Apollo's Legendary Bow";
		attack = 54;
		accuracy = -5;
		equipMessage = "You have equipped " + name
				+ "; your blood courses with the power of Apollo!";
		unequipMessage = "You have unequipped Apollo's Bow";
	}
}
