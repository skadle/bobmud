package model.items;

public class BeginnerChest extends ModelChest {

	public BeginnerChest() {
		super();
		id = "plate";
		setDefaults();
	}

	public BeginnerChest(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		desc = "This suit of plate armor is somewhat sturdy, but rather cumbersome.";
		name = "a suit of plate armor";
		defense = 5;
		agility = -10;
		protectLocation ="chest";
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
