package model.items;

public class BeginnerShoe extends ModelBoots {

	public BeginnerShoe() {
		super();
		id = "shoes";
		setDefaults();
	}

	public BeginnerShoe(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "A pair of unimpressive shoes, held together with ragged string laces.";
		name = "a pair of shoes";
		defense = 1;
		protectLocation = "feet";
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
