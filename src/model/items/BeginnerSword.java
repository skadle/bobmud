package model.items;

public class BeginnerSword extends ModelWeapon {

	public BeginnerSword(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public BeginnerSword() {
		super();
		id = "sword";
		setDefaults();
	}

	public void setDefaults() {
		desc = "This sword is best suited to amateur combatants.";
		name = "a beginning sword";
		attack = 3;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
