package model.items;


public class IntermediateChest extends ModelArmor{

	public IntermediateChest() {
		super();
		id = "TemplarChest";
		setDefaults();
	}

	public IntermediateChest(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		desc = "This templar's plate armor is sturdy, but it is heavy.";
		name = "templar's plate armor";
		defense = 15;
		agility = -7;
		protectLocation ="chest";
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
