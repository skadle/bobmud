package model.items;

public class WarmongerChest extends ModelChest {

	public WarmongerChest(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public WarmongerChest() {
		super();
		id = "chest";
		setDefaults();
	}

	public void setDefaults() {
		name = "The Warmonger Chest";
		desc = "A legendary chest plate called Warmonger.";
		defense = 23;
		agility = -15;
		equipMessage = "You have equipped Warmonger Chest; you feel unstoppable!";
		unequipMessage = "You have unequipped the Warmonger Chest.";
	}

}
