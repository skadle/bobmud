package model.items;

import model.Living;

public class MinorHealthPotion extends Resource {

	public MinorHealthPotion(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public MinorHealthPotion() {
		super();
		super.id = "Minor Health Potion";
		setDefaults();
	}

	public void setDefaults() {
		name = "a Minor Health Potion";
		desc = "Minor Health Potion that can be looted from monsters, or puchased from vendors.";
	}

	@Override
	public String execute(Living player) {
		player.changeHP(5);
		return "You have used a minor health potion, and feel a little better.";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}

}
