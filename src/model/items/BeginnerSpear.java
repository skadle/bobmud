package model.items;

public class BeginnerSpear extends Model2HWeapon {

	public BeginnerSpear(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public BeginnerSpear() {
		super();
		id = "spear";
		setDefaults();
	}

	public void setDefaults() {
		desc = "This spear is best suited to amateur combatants.";
		name = "a rusty spear";
		attack = 5;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}

}
