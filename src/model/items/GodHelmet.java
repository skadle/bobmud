package model.items;

public class GodHelmet extends ModelHelmet {

	public GodHelmet(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GodHelmet() {
		super();
		this.id = "GodHelmet";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodHelmet";
		desc = "This is Hades' Helmet, covering your head so well that you might as well be invisible.";
		equipMessage = "You have equipped Hades' Helmet. You are fearsome!";
		unequipMessage = "You have unequipped Hades' Helmet.";
		agility = 2000;
		protectLocation = "head";
	}
}
