package model.items;

public class DefenseBoots extends ModelBoots {

	public DefenseBoots(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public DefenseBoots() {
		super();
		id = "sabatons";
		setDefaults();
	}

	public void setDefaults() {
		name = "Sabatons of Defense";
		desc = "This legendary pair of sabatons was worn by a knight to cover his feet and shins in thick metal plates.";
		equipMessage = "You have equipped " + name
				+ "; your feet feel much safer.";
		defense = 21;
		agility = 5;
		protectLocation = "feet";
		unequipMessage = "You have unequipped " + name;
	}
}
