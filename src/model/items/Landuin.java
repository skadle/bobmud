package model.items;

public class Landuin extends ModelLeftHand {

	public Landuin(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public Landuin() {
		super();
		id = "Landuin";
		setDefaults();
	}

	public void setDefaults() {
		name = "a large metal shield inscribed \"Landuin\"";
		desc = "A legendary shield called Landuin, no one knows where it is from";
		defense = 20;
		equipMessage = "You have equipped " + name
				+ "; your enemies will be defeated!";
		unequipMessage = "You have unequipped " + name;
	}
}
