package model.items;


public class IntermediateBow extends Model2HWeapon {

	public IntermediateBow() {
		super();
		id = "longbow";
		setDefaults();
	}

	public IntermediateBow(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This longbow is better than shortbow but it is not as good as masters level weapons";
		name = "longbow";
		attack = 34;
		accuracy = -10;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}

