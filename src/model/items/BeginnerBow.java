package model.items;

public class BeginnerBow extends Model2HWeapon {

	public BeginnerBow() {
		super();
		id = "shortbow";
		setDefaults();
	}

	public BeginnerBow(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This shortbow is best suited to amateur combatants.";
		name = "A shortbow";
		attack = 4;
		accuracy = -20;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}

}
