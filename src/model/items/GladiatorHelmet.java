package model.items;

public class GladiatorHelmet extends ModelHelmet {

	public GladiatorHelmet(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GladiatorHelmet() {
		super();
		id = "armet";
		setDefaults();
	}

	public void setDefaults() {
		name = "the Legendary Gladiator's Armet";
		desc = "This full-faced helmet, or armet, is worn by gladiators and men-at-arms and provides excellent face protection.";
		defense = 24;
		protectLocation = "head";
		equipMessage = "You have equipped " + name
				+ "; you feel much safer than before";
		unequipMessage = "You have unequipped " + name;
	}
}
