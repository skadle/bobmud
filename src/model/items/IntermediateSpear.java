package model.items;


public class IntermediateSpear extends ModelWeapon{

		public IntermediateSpear(String id) {
			super(id);
			this.id = id;
			setDefaults();
		}

		public IntermediateSpear() {
			super();
			id = "templarspear";
			setDefaults();
		}

		public void setDefaults() {
			desc = "This Templar's spear is best suited to amateur combatants.";
			name = "a templar's spear";
			attack = 36;
			equipMessage = "You have equipped " + name;
			unequipMessage = "You have unequipped " + name;
		}

}
