package model.items;


public class IntermediateShield extends ModelArmor{

		public IntermediateShield() {
			super();
			id = "steelshield";
			setDefaults();
		}

		public IntermediateShield(String id) {
			super(id);
			this.id = id;
			setDefaults();
		}

		public void setDefaults() {
			desc = "This steel shield is best suited to combatants.";
			name = "a steel shield";
			defense = 12;
			equipMessage = "You have equipped " + name;
			unequipMessage = "You have unequipped " + name;
		}
}
