package model.items;

public class GodTrident extends Model2HWeapon {

	public GodTrident(String id) {
		super(id);
		this.id=id;
		setDefaults();
	}

	public GodTrident() {
		super();
		id = "GodTrident";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodTrident";
		desc = "A legendary spear that was used by Poseidon, it was told that this trident can cause earthquakes.";
		attack = 2500;
		accuracy = 150;
		equipMessage = "You have equipped Poseidon's Trident in both hands!";
		unequipMessage = "You have unequipped Poseidon's Trident";
	}
}
