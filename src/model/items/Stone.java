package model.items;

import model.Living;

public class Stone extends Tool {

	public Stone(String temp) {
		super();
		this.id = "Stone";
		setDefaults();
	}

	public Stone() {
		super();
		id = "stone";
		setDefaults();
	}

	public void setDefaults() {
		name = "a stone";
		desc = "A stone may be useful in some occasions...";
	}

	@Override
	public String execute(Living player) {
		return "You do not know what to do with stone, so you put it back in your inventory";
	}

	@Override
	public String unEquip(Living player) {
		return null;
	}

}
