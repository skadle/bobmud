package model.items;

public class GodShield extends ModelLeftHand {

	public GodShield(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GodShield() {
		super();
		id = "GodShield";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodShield";
		desc = "This legendary shield \"Landuin\" was made by Athena, but noone knows how it got here.";
		defense = 200;
		attack = 70;
		equipMessage = "You have equipped Athena's Shield. You feel formidable!";
		unequipMessage = "You have unequipped Athena's Shield.";
	}
}