package model.items;

import model.Living;

public class Torch extends Tool {

	public Torch(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public Torch() {
		super();
		super.id = "torch";
		setDefaults();
	}

	public void setDefaults() {
		name = "a torch";
		desc = "This is a torch but it seems useless.";
	}

	@Override
	public String execute(Living player) {
		// TODO Auto-generated method stub
		return "You have used torch.";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}

}
