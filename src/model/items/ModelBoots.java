package model.items;

public class ModelBoots extends ModelArmor {
	public ModelBoots() {
		super();
		setDefaults();
	}

	public ModelBoots(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		equipFail = "You are already wearing something on your feet.";
		protectLocation = "feet";
	}
}