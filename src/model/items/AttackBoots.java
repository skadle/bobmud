package model.items;

public class AttackBoots extends ModelBoots {

	public AttackBoots() {
		super();
		id = "boots";
		setDefaults();
	}

	public AttackBoots(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "These steel-toed boots would be great for attacking your enemies!";
		name = "Attack Boots";
		defense = 17;
		attack = 10;
		accuracy = 5;
		equipMessage = "You have equipped " + name
				+ "; you feel ready to take on anyone or anything!";
		unequipMessage = "You have unequipped Attack Boots";
	}
}
