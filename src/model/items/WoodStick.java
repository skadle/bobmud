package model.items;

import model.Living;

public class WoodStick extends Tool {

	public WoodStick(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public WoodStick() {
		super();
		id = "stick";
		setDefaults();
	}

	public void setDefaults() {
		name = "a wooden stick";
		desc = "A wood stick may come in handy...";
	}

	@Override
	public String execute(Living player) {
		// TODO Auto-generated method stub
		return "You stick the stick into your belt until it is stuck.";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return "You remove the stick from your belt.";
	}
}
