package model.items;

import model.Item;
import model.Living;

public class Corpse extends Item {

	public Corpse(String id) {
		super(id = "corpse of " + id);
		this.id = id;
		setDefaults();
	}

	public Corpse() {
		super();
		id = "corpse";
		setDefaults();
	}

	public void setDefaults() {
		desc = "A nasty, bloody, bloated corpse.";
		name = "a corpse";
		if (!id.equals("corpse")) {
			String temp = id.substring(10);
			desc = "The nasty, bloody, bloated corpse of " + temp;
			name = "the corpse of " + temp;
		}
	}

	@Override
	public String execute(Living living) {
		// TODO Auto-generated method stub
		return null;
	}
}
