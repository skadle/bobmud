package model.items;

public class CaptainJackGun extends ModelWeapon {

	public CaptainJackGun(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public CaptainJackGun() {
		super();
		id = "repeater";
		setDefaults();
	}

	public void setDefaults() {
		desc = "This is the lengendary repeating musket used by Captain Jack!";
		name = "Captain Jack's Repeater";
		attack = 52;
		accuracy = -7;
		equipMessage = "You have equipped the lengendary weapon" + name + "!";
		unequipMessage = "You have unequipped " + name;
	}
}
