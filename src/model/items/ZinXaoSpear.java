package model.items;


public class ZinXaoSpear extends Model2HWeapon {

	public ZinXaoSpear(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public ZinXaoSpear() {
		super();
		id = "ZinXaoSpear";
		setDefaults();
	}

	public void setDefaults() {
		name = "ZinXaoSpear";
		desc = "A legendary spear that was used by famous Zin Xao, it was told that Zin Xao defeated thousands of enemy with this spear.";
		equipMessage = "You have equipped the lengendary weapon Zin Xao's Spear!";
		unequipMessage = "You have unequipped Zin Xao's Spear";
		attack = 57;
	}
}
