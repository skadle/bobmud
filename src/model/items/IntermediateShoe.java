package model.items;


public class IntermediateShoe extends ModelArmor{

		public IntermediateShoe() {
			super();
			id = "leathershoes";
			setDefaults();
		}

		public IntermediateShoe(String id) {
			super(id);
			this.id = id;
			setDefaults();
		}

		public void setDefaults() {
			desc = "A pair of leather shoes, held together with ragged string laces.";
			name = "a pair of leather shoes";
			defense = 11;
			protectLocation = "feet";
			equipMessage = "You have equipped " + name;
			unequipMessage = "You have unequipped " + name;
		}


}
