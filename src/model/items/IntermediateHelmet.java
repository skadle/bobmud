package model.items;


public class IntermediateHelmet extends ModelArmor{
	
		public IntermediateHelmet() {
			super();
			id = "platehelmet";
			setDefaults();
		}

		public IntermediateHelmet(String id) {
			super(id);
			this.id = id;
			setDefaults();
		}

		public void setDefaults() {
			desc = "This English plate helmet is best suited to combatants.";
			name = "a English helmet";
			defense = 14;
			protectLocation = "head";
			equipMessage = "You have equipped " + name;
			unequipMessage = "You have unequipped " + name;
		}

}
