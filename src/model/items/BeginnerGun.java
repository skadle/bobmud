package model.items;

public class BeginnerGun extends ModelWeapon {

	public BeginnerGun() {
		super();
		id = "blunderbuss";
		setDefaults();
	}

	public BeginnerGun(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This blunderbuss is a beginner's gun, suited to amateur combatants. It has a wide muzzle,\n"
				+ "allowing a shooter to shoot just about anything from it.";
		name = "a small blunderbuss";
		attack = 4;
		accuracy = -25;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}

}
