package model.items;

import model.Item;
import model.Living;

abstract public class ModelArmor extends Item {
	protected String protectLocation;

	public ModelArmor() {
		super();
		setDefaults();
	}

	public ModelArmor(String id) {
		super(id);
		this.id = id;
		setDefaults();

	}

	protected void setDefaults() {
		equipFail = "You are already wearing something there.";
//		protectLocation = "chest";
//		id = "modelArmorId";
//		name = "modelArmorName";
//		desc = "modelArmorDesc";
	}

	public String execute(Living player) {
		if (!player.isItemEquipped(protectLocation)) {
			player.setEquipped(true, protectLocation);
			player.addEquipped(id);
			player.changeAttack(attack);
			player.changeAccuracy(accuracy);
			player.changeDefense(defense);
			player.changeAgility(agility);
			return "You have equipped " + name + " on your " + protectLocation
					+ ".";
		} else
			return "You are already wearing something on your "
					+ protectLocation + ".";
	}

	public String unEquip(Living player) {
		player.setEquipped(false, protectLocation);
		player.removeEquipped(id);
		player.changeAttack(-attack);
		player.changeAccuracy(-accuracy);
		player.changeDefense(-defense);
		player.changeAgility(-agility);
		return unequipMessage;
	}
}
