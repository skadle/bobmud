package model.items;

import model.Living;

public class MajorManaPotion extends Resource {

	public MajorManaPotion(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public MajorManaPotion() {
		super();
		super.id = "a Major Mana Potion";
		setDefaults();
	}

	public void setDefaults() {
		name = "a Major Mana Potion";
		desc = "Major Mana Potion that can be looted from monsters, or puchased from vendors";

	}

	@Override
	public String execute(Living player) {
		return "This potion gives you so much MANA!!!";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}

}
