package model.items;

import model.Item;
import model.Living;
import model.Room;

public class Ruby extends Item {

	private Room room;
	
	public Ruby(Room nextRoom){
		super("Ruby");
		this.room = nextRoom;
	}
	
	
	@Override
	public String execute(Living living) {
		if(living.getRoom().getID().equalsIgnoreCase("Dungeon")){
			living.getRoom().addExit("door", room);
			return "You place the ruby in the empty eye of the stature and the wall opens reviling a path to take.";
		}
		return "You can not use that here.";
	}

}
