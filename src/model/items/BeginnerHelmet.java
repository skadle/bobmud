package model.items;

public class BeginnerHelmet extends ModelHelmet {

	public BeginnerHelmet() {
		super();
		id = "helmet";
		setDefaults();
	}

	public BeginnerHelmet(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This small Spanish helmet is best suited to amateur combatants.";
		name = "a Spanish helmet";
		defense = 4;
		protectLocation = "head";
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
