package model.items;

import model.Living;

public abstract class Model2HWeapon extends ModelWeapon {

	public Model2HWeapon() {
		super();
		setDefaults();
	}

	public Model2HWeapon(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		equipMessage = "You have equipped " + name + " , using both hands.";
		equipFail = "You must have two hands free to equip this item.";
	}

	public String execute(Living player) {
		if (!player.isItemEquipped("right hand")
				&& !player.isItemEquipped("left hand")) {
			player.setEquipped(true, "right hand");
			player.setEquipped(true, "left hand");
			player.changeAttack(attack);
			player.changeAccuracy(accuracy);
			player.changeDefense(defense);
			player.changeAgility(agility);
			player.addEquipped(id);
			return equipMessage;
		} else
			return equipFail;
	}

	public String unEquip(Living player) {
		player.setEquipped(false, "right hand");
		player.setEquipped(false, "left hand");
		player.removeEquipped(id);
		player.changeAttack(-attack);
		player.changeAccuracy(-accuracy);
		player.changeDefense(-defense);
		player.changeAgility(-agility);
		return unequipMessage;
	}
}
