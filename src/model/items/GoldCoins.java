package model.items;

import model.Item;
import model.Living;

public class GoldCoins extends Item {
	int value;

	public GoldCoins(String id) {
		super(id);
		this.id = id;
		setDefaults();

		// TODO Auto-generated constructor stub
	}

	public GoldCoins() {
		super();
		id = "coins";
		setDefaults();
	}

	@Override
	public void setDefaults() {
		name = "A pile of gold coins.";
		desc = "A small pile of gold coins is here.";
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

	public void setValue(int v) {
		this.value = v;
	}

	@Override
	public String execute(Living living) {
		// TODO Auto-generated method stub
		return null;
	}

}
