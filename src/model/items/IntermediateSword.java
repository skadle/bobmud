package model.items;

public class IntermediateSword extends ModelWeapon{
	
	public IntermediateSword(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public IntermediateSword() {
		super();
		id = "cutlass";
		setDefaults();
	}

	public void setDefaults() {
		desc = "This sword is best suited to amateur combatants.";
		name = "a cutlass";
		attack = 27;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
