package model.items;


public class FBsword extends ModelWeapon {

	public FBsword(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public FBsword() {
		super();
		id = "FBSword";
		setDefaults();
	}

	public void setDefaults() {
		desc = "A legendary sword also known as \"freaking best sword\", or the FBSword.";
		name = "the legendary FBSword";
		attack = 44;
		equipMessage = "You have equipped " + name
				+ "; you feel unstoppable!";
		unequipMessage = "You have unequipped" + name;
	}
}
