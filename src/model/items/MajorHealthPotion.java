package model.items;

import model.Living;

public class MajorHealthPotion extends Resource {

	public MajorHealthPotion(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public MajorHealthPotion() {
		super();
		id = "Major Health Potion";
		setDefaults();
	}

	public void setDefaults() {
		name = "a Major Health Potion";
		desc = "Major Health Potion that can be looted from monsters, or puchased from vendors.";
	}

	@Override
	public String execute(Living player) {
		player.changeHP(20);
		return "You have used a major health potion, and feel much better!";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}
}
