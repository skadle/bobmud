package model.items;

import model.Item;
import model.Living;

public abstract class ModelLeftHand extends Item {

	public ModelLeftHand() {
		super();
		setDefaults();
	}

	public ModelLeftHand(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		equipMessage = "You have equipped " + name + ".";
		equipFail = "You are already holding something in your right hand.";
	}

	public String execute(Living player) {
		if (!player.isItemEquipped("left hand")) {
			player.setEquipped(true, "left hand");
			player.changeAttack(attack);
			player.changeAccuracy(accuracy);
			player.changeDefense(defense);
			player.changeAgility(agility);
			player.addEquipped(id);
			return equipMessage;

		} else
			return equipFail;
	}

	public String unEquip(Living player) {
		player.setEquipped(false, "left hand");
		player.removeEquipped(id);
		player.changeAttack(-attack);
		player.changeAccuracy(-accuracy);
		player.changeDefense(-defense);
		player.changeAgility(-agility);
		return unequipMessage;
	}
}
