package model.items;


public class BeginnerShield extends ModelLeftHand {

	public BeginnerShield() {
		super();
		id = "shield";
		setDefaults();
	}

	public BeginnerShield(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This small wooden shield is best suited to amateur combatants.";
		name = "a small wooden shield";
		defense = 2;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
