package model.items;

public class ModelChest extends ModelArmor {
	public ModelChest() {
		super();
		setDefaults();
	}

	public ModelChest(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		equipFail = "You are already wearing something on your chest.";
		protectLocation = "chest";
	}

}