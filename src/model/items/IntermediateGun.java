package model.items;


public class IntermediateGun extends ModelWeapon{
	
	public IntermediateGun() {
		super();
		id = "pistol";
		setDefaults();
	}

	public IntermediateGun(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public void setDefaults() {
		desc = "This pistol was often used by pirates for duel";
		name = "pistol";
		attack = 31;
		accuracy = -13;
		equipMessage = "You have equipped " + name;
		unequipMessage = "You have unequipped " + name;
	}
}
