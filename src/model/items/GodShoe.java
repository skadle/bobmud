package model.items;

public class GodShoe extends ModelBoots {

	public GodShoe(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GodShoe() {
		super();
		id = "GodShoes";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodShoes";
		desc = "This is the legendary pair of winged shoes worn by Hermes.";
		agility = 500;
		equipMessage = "You have equipped Hermes' Shoes. You feel like you can fly!";
		unequipMessage = "You have unequipped Hermes' Shoes.";
		protectLocation = "feet";
	}
}
