package model.items;

public class ModelHelmet extends ModelArmor {
	public ModelHelmet() {
		super();
		setDefaults();
	}

	public ModelHelmet(String id) {
		super(id);
		this.id=id;
		setDefaults();
	}

	protected void setDefaults() {
		equipFail = "You are already wearing something on your head.";
	}

}
