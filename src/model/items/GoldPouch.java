package model.items;

import model.Living;

public class GoldPouch extends Resource{
	
	private int gold = 0;

	public GoldPouch() {
		super();
		this.id = "Gold Pouch";
		this.gold = 0;
	}
	
	public GoldPouch(int gold) {
		super();
		this.id = "Gold Pouch";
		this.gold = gold;
	}
	
	public void addGold(int amount){
		gold += amount;
	}
	
	public int getGold(){
		return gold;
	}

	@Override
	public String execute(Living player) {
		return "There is "+ gold +" peices of gold in this pouch.";
	}

	@Override
	public String unEquip(Living player) {
		return "You do not want to drop your gold";
	}

}
