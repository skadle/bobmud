package model.items;

import model.Item;
import model.Living;

public abstract class ModelWeapon extends Item {

	public ModelWeapon() {
		super();
		setDefaults();
	}

	public ModelWeapon(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	protected void setDefaults() {
		equipMessage = "You have equipped " + name + ".";
		equipFail = "You are already holding something in your right hand.";
	}

	public String execute(Living player) {
		if (!player.isItemEquipped("right hand")) {
			player.setEquipped(true, "right hand");
			player.changeAttack(attack);
			player.changeAccuracy(accuracy);
			player.changeDefense(defense);
			player.changeAgility(agility);
			player.addEquipped(id);
			return equipMessage;

		} else
			return equipFail;
	}

	public String unEquip(Living player) {
		player.setEquipped(false, "right hand");
		player.removeEquipped(id);
		player.changeAttack(-attack);
		player.changeAccuracy(-accuracy);
		player.changeDefense(-defense);
		player.changeAgility(-agility);
		return unequipMessage;
	}
}
