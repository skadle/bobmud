package model.items;

import model.Living;

abstract public class Tesource extends Resource {

	public Tesource(String id) {
		super();
		this.id = id;
		setDefaults();
	}

	public Tesource() {
		super();
		id = "tesource";
		setDefaults();
	}

	public void setDefaults() {
		name = "a tesource";
		desc = "A mythical tesource... Only Sung knows what it's for.";
	}

	public String execute(Living player) {

		return "You have used a tesource! The Sung smiles down at you.";
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}
}
