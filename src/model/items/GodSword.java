package model.items;

public class GodSword extends ModelWeapon {

	public GodSword(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GodSword() {
		super();
		id = "GodSword";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodSword";
		desc = "This is the legendary Sword of Zeus called \"The Blade of Olympus\".";

		attack = 200;
		accuracy = 100;
		equipMessage = "You have equipped Zeus' Sword, \"The Blade of Olympus\".";
		unequipMessage = "You have unequipped Zeus' Sword but why....";
	}
}
