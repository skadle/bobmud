package model.items;

public class GodArmor extends ModelChest {

	public GodArmor(String id) {
		super(id);
		this.id = id;
		setDefaults();
	}

	public GodArmor() {
		super();
		id = "GodArmor";
		setDefaults();
	}

	public void setDefaults() {
		name = "GodArmor";
		desc = "This armor was made for Achilles by the Greek god Hephaestus and provides perfect chest protection.";
		equipMessage = "You have equipped Achilles' Chest Plate and you feel unstoppable!";
		unequipMessage = "You have unequipped Achilles' Chest Plate.";
		defense = 2000;
		agility = 500;
		protectLocation = "chest";
	}
}
