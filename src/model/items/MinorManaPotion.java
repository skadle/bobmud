package model.items;

import model.Living;

public class MinorManaPotion extends Resource{
	
	private String desc = "Minor Mana Potion that can be looted from monsters, or puchased from vendors";

	public MinorManaPotion(String id) {
		super();
		super.id = "Minor Mana Potion";
		this.setDesc(desc);
	}
	
	public MinorManaPotion() {
		super();
		super.id = "Minor Mana Potion";
		this.setDesc(desc);
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String execute(Living player) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String unEquip(Living player) {
		// TODO Auto-generated method stub
		return null;
	}

}
