package model;

import java.io.Serializable;
import java.util.HashMap;

public class PasswordSave implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	HashMap<String, String> passwords = new HashMap<String, String>();

	public PasswordSave(HashMap<String, String> passwords) {
		this.passwords = passwords;
	}

	public HashMap<String, String> getMap() {
		return passwords;
	}

}
