package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Empire extends NormalRoom {

	public Empire(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A gangplank to the HMS Empire";
		desc = "    Easily the tallest ship in cove, this massive galleon is renowned for\n"
				+ "her speed, her guns, and her mission - to hunt down and exterminate all\n"
				+ "pirates in the Caribbean Sea.\n"
				+ "You stand upon the gangplank before a burly pirate who denies your advances.";
		nouns.put("empire", desc);
		nouns.put("HMS empire", desc);
		nouns.put("galleon", desc);
		nouns.put("ship", desc);
		String gangplank = "The gangplank lets sailors pass between the galleon and the pier.";
		nouns.put("gangplank", gangplank);
		nouns.put("pirate", "The burly pirate will not let you on the ship.");
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
