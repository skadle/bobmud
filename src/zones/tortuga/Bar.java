package zones.tortuga;

import java.util.ArrayList;

public class Bar extends VendorRoom {

	public Bar(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!
		String sing = "The pirates sing, \"YO HO YO HO A PIRATE'S LIFE FOR ME!!\"";
	}

	@Override
	public void setDefaults() {
		name = "A rowdy bar";
		desc = "    This public house is home to the most dangerous, most nefarious, and most\n"
				+ "boisterous crowd every to grace a brew house! Pirates jostle around you, nearly\n"
				+ "spilling copious amounts of beer and spirits everywhere.";
		nouns.put("bar", desc);
		nouns.put("public house", desc);
		String pirates = "Most of the pirates join in a baudy song of wooing a famous Spanish princess.";
		nouns.put("pirates", pirates);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
