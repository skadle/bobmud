package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaCenter extends NormalRoom {

	public TortugaCenter(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "The center of Tortuga";
		desc = "    The cobblestone street connecting everything to everything.";
		nouns.put("street", desc);
		String cobblestone = "The cobblestones are worn slick from two-hundred years of tread.";
		nouns.put("cobblestone", cobblestone);
		nouns.put("cobblestones", cobblestone);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
