package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Docks extends NormalRoom {

	public Docks(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "The docks";
		desc = "Numerous dinghys and pinnaces are tied up here, as well as two galleons.";
		nouns.put("docks", desc);
		String galleons = "    The fearsome pirate galleon Flying Dutchman is anchored alongside\n"
				+ "the feared English pirate-hunting galleon, the HMS Empire.";
		nouns.put("galleons", galleons);
		nouns.put("Flying Dutchman", galleons);
		nouns.put("HMS Empire", galleons);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
