package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Pier extends NormalRoom {

	public Pier(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A broken pier";
		desc = "This pier is derelict and sinks sharply beneath the water.";
		nouns.put("pier", desc);
		String water = "The water is crystal clear. The pier has toppled into it.";
		nouns.put("water", water);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
