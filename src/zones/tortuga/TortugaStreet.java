package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaStreet extends NormalRoom {

	public TortugaStreet(String id) {
		super(id);
		setDefaults();
		setName("A winding street through Tortuga.");
		setDesc("    This cobblestone avenue of Tortuga's main thoroughfare winds between\n"
				+ "all sorts of close-packed shantys. Myriad narrow alleys lead off to who\n"
				+ "knows where. Wooden planks take you across dark pools of still water. Huge\n" +
				"willow branches hang precariously over the street, casting deep shadows over\n" +
				"everything.");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
