package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaWest extends NormalRoom {

	public TortugaWest(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A winding street through Tortuga";
		desc = "    This cobblestone avenue off Tortuga's main thoroughfare winds between\n"
				+ "all sorts of close-packed shantys. Myriad narrow alleys lead off to who\n"
				+ "knows where. Wooden planks take you across dark pools of still water. Huge\n"
				+ "willow branches hang precariously over the street, casting deep shadows over\n"
				+ "everything.";
		nouns.put("street", desc);
		String planks = "    Dangerous, creaking planks serve as a makeshift walkway across streams\n"
				+ "that have cut deep ruts in the street. The wood bends and shifts precariously as\n"
				+ "you carefully cross them.";
		nouns.put("planks", planks);
		nouns.put("wooden planks", planks);
		String streams = "Ground water from the jungle cuts through the streets to the gutters.";
		nouns.put("streams", streams);
		nouns.put("ruts", streams);
		nouns.put("gutters",
				"The water flows into gutters, and down into the covered sewer.");
		nouns.put("sewer", "The sewer is where the water flows.");
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
