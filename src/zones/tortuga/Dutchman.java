package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Dutchman extends NormalRoom {

	public Dutchman(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A gangplank to the Flying Dutchman";
		desc = "    You stand upon the gangplank of the Flying Dutchman. A burly pirate shakes\n"
				+ "his head at you as you try to board the infamous pirate galleon.";
		nouns.put("gangplank", desc);
		String dutchman = "The infamous Flying Dutchman pirate galleon is at anchor here.";
		nouns.put("dutchman", dutchman);
		nouns.put("flying dutchman", dutchman);
		nouns.put("galleon", dutchman);
		nouns.put("pirate galleon", dutchman);
		nouns.put("pirate", "The burly pirate will not let you on the ship.");
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
