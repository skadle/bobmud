package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaSouth extends NormalRoom {

	public TortugaSouth(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDefaults() {
		name = "The south street of Tortuga";
		desc = "    The southern street leads from the center of town to Father Cook's mission.";
		nouns.put("street", desc);
		String mission = "The mission is a church started by Father Cook.";
		nouns.put("mission", mission);
		nouns.put("church", mission);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
