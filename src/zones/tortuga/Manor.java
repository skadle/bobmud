package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Manor extends NormalRoom {

	public Manor(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "Captain Tully's Manor";
		desc = "This is the austere residence of the infamous dread pirate Tully. He has a clock.";
		nouns.put("manor", desc);
		nouns.put("residence", desc);
		String clock = "Tick tock, time is ticking, hurry up!";
		nouns.put("clock", clock);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
