package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Warehouse extends NormalRoom {

	public Warehouse(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A warehouse";
		desc = "The large, wooden warehouse is stacked with crates full of stolen commodities.";
		nouns.put("warehouse", desc);
		String commodities = "Pirates who accumulate too much plunder store their excess here.";
		nouns.put("commodities", commodities);
		nouns.put("plunder", commodities);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
