package zones.tortuga;

import java.util.ArrayList;

import model.Living;
import model.livings.Player;
import model.rooms.NormalRoom;

public class Mission extends NormalRoom {

	public Mission(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		String drip = "Drip.";
		String plop = "Plop.";
		// TODO Auto-generated method stub

	}

	@Override
	public void setDefaults() {
		name = "Father Cook's Mission";
		desc = "    The mission has seen better days. The roof is missing, which is nice\n"
				+ "on sunny days, but not so good for the rain.  There are no pews, as those\n"
				+ "were plundered by the laziest and mean-spirited of pirates long ago. A small\n"
				+ "font of holy water sit near the entrance.";
		nouns.put("church", desc);
		String roof = "The roof is missing. Father Cook would say, \"Only God lives above us.\"";
		nouns.put("roof", roof);
		String font = "    It is customary to sanctify oneself with the water upon entering. A drop\n"
				+ "of water splashes into the font from beneath a hanging gargoyle overhead.";
		nouns.put("font", font);
		nouns.put("holy water", font);
		nouns.put("water", font);
		String spigot = "An iron spigot carefully funnels rain water into the font from above.";
		nouns.put("gargoyle", spigot);
		nouns.put("spigot", spigot);
		nouns.put("rain water", spigot);
	}

	// Player can sanctify another living in the room, echo appropriately
	// Player can sanctify themself, echo appropriately
	public void sanctify(Player sprinkler, Living target) {
		// if sprinkler != target and target is a player, echo to target
		if (sprinkler != target && target instanceof Player) {
			target.echo("You are sprinkled with holy water and your sins are washed clean.");
		}// if sprinkler == target, echo to target
		else if (sprinkler == target) {
			target.echo("You sprinkle yourself with holy water and your sins are washed clean.");
		}
		// iterate over the livings to see who to echo to
		for (int i = 0; i < livings.size(); i++) {
			// if sprinkler sprinkles someone else, echo to sprinkler
			if (livings.get(i) == sprinkler && sprinkler != target) {
				sprinkler.echo("You sprinkle " + target.getShortName()
						+ " with holy water and " + target.getShortName()
						+ "'s sins are washed clean.");
			}// find players in the room other than sprinkler and target
			else if (livings.get(i) instanceof Player
					&& (livings.get(i) != sprinkler && livings.get(i) != target)) {
				// if sprinkler != target, echo to other players
				if (sprinkler != target) {
					livings.get(i).echo(
							sprinkler.getShortName() + " flicks water at "
									+ target.getShortName() + ".");
				} // if sprinkler is the target, echo to other players
				else
					livings.get(i)
							.echo(sprinkler.getShortName()
									+ "splashes around in the font of holy water.");
			}

		}
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
