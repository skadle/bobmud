package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaBurnedBuilding extends NormalRoom {

	public TortugaBurnedBuilding(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A fire-gutted house";
		desc = "What was once a nice manor or warehouse along the north docks is not just a ruin.\n";
		nouns.put("ruin", desc);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
