package zones.tortuga;

import java.util.ArrayList;


public class TradingPost extends VendorRoom {

	public TradingPost(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A cramped trading post";
		desc = "The owner is nowhere to be found. The store must be closed. Oh well.";
		nouns.put("store", desc);
		nouns.put("trading post", desc);
		String owner = "The owner is not here. Must be too busy to make a living.";
		nouns.put("owner", owner);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
