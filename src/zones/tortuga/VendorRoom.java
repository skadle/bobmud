package zones.tortuga;

import java.util.HashMap;

import model.Item;
import model.Room;

public abstract class VendorRoom extends Room{
	
	protected HashMap<String, Item> itemList;

	public VendorRoom(String id) {
		super(id);
		itemList = new HashMap<String, Item>();
		// TODO Auto-generated constructor stub
	}
	
	public void addItem(Item item){
		itemList.put(item.getName(), item);
	}
	
	

}
