package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaEast extends NormalRoom {

	public TortugaEast(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "The east streets of Tortuga";
		desc = "    The town of Tortuga bustles around you on the busiest boulevard.\n"
				+ "Captain Tully's mansion is to the north, the docks to the east, and\n"
				+ "commercial shops to the south.";
		nouns.put("street", desc);
		nouns.put("boulevard", desc);
		String mansion = "The largest house in Tortuga, that of the infamous Captain Tully.";
		nouns.put("mansion", mansion);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
