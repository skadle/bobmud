package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Graveyard extends NormalRoom {

	public Graveyard(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDefaults() {
		name = "Tortuga's Cemetery";
		desc = "    A wrought-iron fence surround the hidden cemetery behind the church.\n"
				+ "Moss and ivy hang from the rocks beyond the fence, and sparse underbrush\n"
				+ "decorates the yard. The local pirates choose to do most of their burials\n"
				+ "at sea; fittingly, there are only a handful of graves here. However there\n"
				+ "are many other ways to honor the dead here: carved wooden crosses honor a\n"
				+ "Catholic soul, and spoons of honey honor another belief. Small candles sit\n"
				+ "atop a stone altar in the corner.";
		nouns.put("cemetery", desc);
		nouns.put("graveyard", desc);
		nouns.put("yard", "The graveyard covers the entire yard.");
		String graves = "Six gravestones stand in of the yard. One grave seems fairly fresh.";
		nouns.put("graves", graves);
		nouns.put("grave",
				"This grave says \"Atocha\" and is dated September 5 - 1622, only three weeks ago.");
		String fence = "    The wrought-iron fence seems unnecessary as stone cliffs surround the\n"
				+ "small graveyard on three sides, and the church wall on the fourth.";
		nouns.put("fence", fence);
		nouns.put("iron fence", fence);
		nouns.put("wrought-iron fence", fence);
		String crosses = "";
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
