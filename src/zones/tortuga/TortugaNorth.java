package zones.tortuga;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TortugaNorth extends NormalRoom {

	public TortugaNorth(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "The north streets of Tortuga";
		desc = "This road leads into the jungle north of Tortuga.";
		nouns.put("road", desc);
		nouns.put("streets", desc);
		String jungle = "That same dark, foreboding, dangerous jungle you remember so fondly.\n";
		nouns.put("jungle", jungle);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
