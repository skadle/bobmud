package zones.sewers;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class TowerBasement extends NormalRoom {

	public TowerBasement(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public void setDefaults() {
		name = "A hidden basement beneath the tower";
		desc = "    After a long climb down the rickety ladder, you find yourself in a dimly lit basement.";
		nouns.put("room", desc);
		String ladder = "A wooden ladder leads up to the attic far above.";
		nouns.put("ladder", ladder);
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
