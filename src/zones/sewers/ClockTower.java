package zones.sewers;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class ClockTower extends NormalRoom {

	public ClockTower(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public void setDefaults() {
		name = "The top of the clock tower";
		desc = "    You climb in through the window from the street, straining to pull yourself into the room.\n"
				+ "As you clamber into the tower, you accidentally unwedge the rope from its catching point, and it falls\n"
				+ "to the street below.\n"
				+ "    Between the cobweb-covered wooden walls, the rickety floor, and the crumbling ceiling, you get the\n"
				+ "impression that this is a dangerous place to hang out. A large brazier sits in the middle of the floor,\n"
				+ "glowing from the heat of the coals within. Two severed hands roast within the brazier, burning with a strange\n"
				+ "blue flame, but never fully charring. The brazier itself gives off no smoke, but the walls are adorned with\n"
				+ "burning incense holders, filling the room with a light haze, and a distinct odor of swamp muck.\n"
				+ "A ladder on the north wall leads down.";
		nouns.put("room", desc);
		String window = "The view is unobstructed all the way past the docks, the bay, and out to the great sea beyond.";
		nouns.put("window", window);
		nouns.put("sea", window);
		nouns.put("docks", window);
		nouns.put("bay", window);
		String rope = "The rope has fallen to the street below.";
		nouns.put("rope", rope);
		nouns.put("street", rope);
		String brazier = "The brazier burns with a strange blue flame. Within rests a pair of severed human hands.";
		nouns.put("brazier", brazier);
		String hands = "Two severed hands burn within the brazier, though the fire does not consume them.";
		nouns.put("hands", hands);
		String incense = "Brass incense holders hang from the four walls, emitting a steady stream of pungent-smelling smoke.";
		nouns.put("incense", incense);
		nouns.put("haze", incense);
		nouns.put("smoke", incense);
		String ladder = "A wooden ladder leads down to the floor far below.";
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
