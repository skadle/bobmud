package zones.sewers;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Cellar extends NormalRoom {

	public Cellar(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!
		String ash = "Ash hangs thick in the air.";
	}

	@Override
	public void setDefaults() {
		name = "An abandoned celler";
		desc = "    The celler beneath the burned building is abandoned, but luckily spared\n"
				+ "of the fire's destruction above. Ash litters the floor, between an assortment\n"
				+ "of ash-covered furniture and various crates and barrels.";
		nouns.put("celler", desc);
		String ash = "Frikkin ash is everywhere!";
		nouns.put("ash", ash);
		nouns.put("everywhere", ash);
		String crates = "These crates and barrels might be a plot hook soon.";
		nouns.put("crates", crates);
		nouns.put("barrels", crates);
		String furniture = "It's a couch, perfect sized for a red herring.";
		nouns.put("furniture", furniture);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
