package zones.general;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class Jungle extends NormalRoom {

	public Jungle(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

	}

	@Override
	public void setDefaults() {
		name = "The dark jungle";
		desc = "    You are inside the deep jungle of the island. Few explorers live to tell\n"
				+ "of what they encounter when wandering the dark interior of the islands. The\n"
				+ "jungle is deathly quiet, the silence broken only by the distant screams of\n"
				+ "a chimp or the low growl of a hidden predator.\n"
				+ "There is a faint trail here, leading through the woods.\n"
				+ "You imagine eyes watching you from the trees.";
		nouns.put("jungle", desc);
		nouns.put("dark jungle", desc);
		String trees = "The trees crowd in around you on all sides, looming overhead.";
		nouns.put("trees", trees);
		nouns.put("trail",
				"There is a faint trail here, leading through the woods.");
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
