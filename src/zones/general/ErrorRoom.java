package zones.general;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class ErrorRoom extends NormalRoom {

	public ErrorRoom(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

	}

	@Override
	public void setDefaults() {
		name = "The error room";
		desc = "    You have come to this room because of an error in the game."
				+ "As soon as possible, please tell the developers how you got here.";
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
