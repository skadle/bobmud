package zones.burningForest;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class BurningCave extends NormalRoom {

	public BurningCave(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A smoky cave";
		desc = "    Walls of natural stone surround you. The floor is covered in damp leaves and\n"
				+ "puddles of slippery water. A pile of leaves in the corner catches your eye.";
		nouns.put("cave", desc);
		String leaves = "The slippery ground is lousy with leaves. They're difficult to stand on.";
		nouns.put("leaves", leaves);
		nouns.put("ground", leaves);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
