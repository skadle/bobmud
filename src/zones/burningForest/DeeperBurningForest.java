package zones.burningForest;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class DeeperBurningForest extends NormalRoom {

	public DeeperBurningForest(String id) {
		super(id);
		setDefaults();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

		String cough = "You cough and choke on the thick, black smoke.";
		String heat = "The heat from the burning forest is intense!";
		String branch = "A flaming branch falls from the canopy above!";
		String run = "The inferno rages about you! RUN!!";
		String breathe = "You can't breathe!";

	}

	@Override
	public void setDefaults() {
		name = "Everything around you is on fire!";
		desc = "    This is hardly a forest anymore, more like a vision of hell! The canopy above\n"
				+ "you is hardly visible through the smoke. The inferno above has spread to the forest\n"
				+ "floor, leaving you in a very precarious position. If you tarry too long in this wood,\n"
				+ "you will surely die!";
		nouns.put("forest", desc);
		String canopy = "The inferno engulfs the trees. The fire is so intense, you can't barely see anything!";
		nouns.put("canopy", canopy);
		nouns.put("smoke",
				"The thick black smokes obscures everything, and you can't breathe!");
		String trees = "A fool can't see the forest for the trees, but you can't see the trees for the inferno!";
		nouns.put("trees", trees);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
