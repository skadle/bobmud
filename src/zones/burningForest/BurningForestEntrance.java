package zones.burningForest;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class BurningForestEntrance extends NormalRoom {

	public BurningForestEntrance(String id) {
		super(id);
		setDefaults();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

		String cough = "You cough and choke on the thick, black smoke.";
		String heat = "The heat from the burning forest is intense!";
		String animals = "A flood of animals runs by you, desperate to escape the inferno!";
		String branch = "A flaming branch falls from the canopy above!";
		String screams = "The panicked screams of fleeing forest critters is deafening!";

	}

	@Override
	public void setDefaults() {
		name = "The forest around you is on fire!";
		desc = "    The forest around you is on fire! Flames engulf the canopy above!\n"
				+ "Flaming branches and leaves fall from above, raining sure doom upon any\n"
				+ "soul unfortunate enough to walk beneath. The forest floor is damp enough\n"
				+ "from the recent rains that the fire is mostly contained above, save for\n"
				+ "the burning branches that have fallen down around you. Forest critters of\n"
				+ "all sorts run panicked through the smoke, chittering, roaring, all seemingly\n"
				+ " screaming the obvious at you: Run for your life, you idiot!\n"
				+ "You cough and choke on the thick smoke.";
		nouns.put("forest", desc);
		String canopy = "Flames engulf the canopy above. Flaming branches rain down around you!";
		nouns.put("canopy", canopy);
		nouns.put("branches", canopy);
		String trees = "The trees-tops burn above you. Fortunately, the fire has not spread much lower.";
		nouns.put("trees", trees);
		String critters = "All sorts of critters run through the forest in a panic.";
		nouns.put("animals", critters);
		nouns.put("critters", critters);
		nouns.put("forest critters", critters);
		nouns.put("birds", critters);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
