package zones.mountain;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class MountainPath extends NormalRoom {

	public MountainPath(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {
		// TODO: chitter chatter!

	}

	@Override
	public void setDefaults() {
		name = "A path along the rocky shore";
		desc = "This path winds between the large jagged rocks along the shore north of Tortuga.";
		nouns.put("path", desc);
		String rocks = "More like boulders!";
		nouns.put("rocks", rocks);
		nouns.put("boulders", rocks);
		String tortuga = "You catch a glimpse of Tortuga farther south from here, around the cove.";
		nouns.put("tortuga", tortuga);
		String cove = "Numerous tall ships dot the crystal waters of the cove.";
		nouns.put("cove", cove);
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
