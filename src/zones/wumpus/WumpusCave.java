package zones.wumpus;

import java.util.ArrayList;

import model.Living;
import model.Room;

public class WumpusCave extends Room{

	public WumpusCave(String id) {
		super("Wumpus Cave");
	}
	
	public WumpusCave() {
		super("Wumpus Cave");
	}
	
	@Override
	protected void setDefaults() {
		// AllRooms.add(this);
		name = "Wumpus Cave";
		desc = "You will need to find a key to get out of here.";
		
	}
	
	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Room move(String direction, Living living) {
		// TODO: move to a different room
		
		if(direction.equalsIgnoreCase("cellar")){
			living.getInventory().contains("key");
			livings.remove(living);
			exits.get(direction).addLiving(living);
			return exits.get(direction);
		}
		return this;
	}
}
