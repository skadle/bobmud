package zones.wumpus;

import java.util.ArrayList;

import model.Room;

public class MercerLair extends Room{
	

		public MercerLair(String id) {
			super("Mercer's Lair");
			// TODO Auto-generated constructor stub
		}
		
		public MercerLair() {
			super("Mercer's Lair");
			// TODO Auto-generated constructor stub
		}

		@Override
		public void chatter() {

		}

		@Override
		public void setDefaults() {
			name = "Mercer's Lair";
			desc = "";
			nouns.put("jungle", desc);
			nouns.put("dark jungle", desc);
			String trees = "The trees crowd in around you on all sides, looming overhead.";
			nouns.put("trees", trees);
			nouns.put("trail",
					"There is a faint trail here, leading through the woods.");
		}

		@Override
		public String useCommand(ArrayList<String> command) {
			// TODO Auto-generated method stub
			return null;
		}
}
