package zones.wumpus;

import java.util.ArrayList;

import model.Room;

public class Dungeon2 extends Room{
		
		public Dungeon2(){
			super("Dungeon2");
		}
		
		@Override
		public void setDefaults() {
			name = "Dungeon2";
			desc = "You notice that moving forward with this adventure may be problematic as you look into a cave with winding and twisting tunnels you wonder if you go down that path will you ever find your way out.";
			
		}

		@Override
		public void chatter() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String useCommand(ArrayList<String> command) {
			return null;
		}

	

}
