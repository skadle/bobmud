package zones.wumpus;

import model.Living;
import model.items.Tool;

public class WumpusKey extends Tool {

	public WumpusKey(String id) {
		super();
		this.id = "key";
		setDefaults();
	}

	public WumpusKey() {
		super();
		id = "key";
		setDefaults();
	}

	public void setDefaults() {
		name = "a slimy green key";
		desc = "This small key is pitted and covered in slime, as if something has been chewing on it.";
	}

	@Override
	public String execute(Living living) {
		
		living.removeEquipped("key");
		return "You unlock the door with the " + name + ".";
	}
}
