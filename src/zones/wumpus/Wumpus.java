package zones.wumpus;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.Item;
import model.Room;
import model.items.GoldPouch;
import model.items.Stone;
import model.items.Tesource;
import model.items.WoodStick;
import model.livings.Creature;

public class Wumpus extends Creature {

	private List<Item> root = new LinkedList<Item>();
	private Item temp = new WoodStick();
	private Item temp2 = new Stone();
//	private Item temp3 = new Tesource();

	public Wumpus(Room room) {
		super("Wumpus", room);
		hp = 70;
		maxHp = hp;
		attack = 20;
		Random randomGenerator = new Random();
		int gold = randomGenerator.nextInt(100);
		Item goldPouch = new GoldPouch(gold + 50);
		root.add(temp);
		root.add(temp2);
//		root.add(temp3);
		root.add(goldPouch);
		int ran = randomGenerator.nextInt(root.size());
		items.add(root.get(ran));
		items.add(new WumpusKey());
		items.add(goldPouch);
	}

	public int getHp() {
		return hp;
	}

	public int getAttack() {
		return attack;
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

}
