package zones.wumpus;

import java.util.ArrayList;

import model.Living;
import model.Room;

public class Pit extends Room {

	public Pit(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}
	
	public Pit(){
		super("pit");
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Room move(String direction, Living living) {
		// TODO: move to a different room
		livings.remove(living);
		exits.get(direction).addLiving(living);
		return exits.get(direction);
	}

}
