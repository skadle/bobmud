package zones.wumpus;

import java.util.ArrayList;

import model.Room;

public class Lost extends Room{

	public Lost(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}
	public Lost(){
		super("lost");
	}
	
	@Override
	public void setDefaults() {
		name = "lost";
		desc = "This room is very dark you can�t seem to see anything.";
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}

}
