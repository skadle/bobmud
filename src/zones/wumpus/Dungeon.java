package zones.wumpus;

import java.util.ArrayList;

import model.Item;
import model.Room;
import model.items.Ruby;

public class Dungeon  extends Room{
	private Room exit;

	public Dungeon(String id, Room exit) {
		super("Dungeon");
		commands.add("pull");
		this.exit = exit;
	}
	
	public Dungeon(Room exit){
		super("Dungeon");
		this.exit = exit;
	}
	
	@Override
	public void setDefaults() {
		name = "Dungeon";
		desc = "You enter a damp dreary dungeon the first thing you notice is the\n" +
				"smell of rotting flesh mixed with moss that looks like it has\n" +
				"not been disturbed in years. The moss is growing up the walls\n" +
				"almost covering a skeleton hanging from shackles. In one corner\n" +
				"of the room there is a statue of the of a some sort of a demonic\n" +
				"creature. In another corner there is a lever in the corner of the room.\n" +
				"It appears to be a dead end.";
		nouns.put("skeleton", "He has seen better days.");
		nouns.put("lever","An old rusty lever it looks like it could still work.");
		nouns.put("moss", "The moss is growing up and down every wall.");
		nouns.put("statue", "The statue is of some sort of hideous creature the likes of which you have never seen before. One of the eyes of the creature is a blood read ruby and the other is an empty socket. At the bottom of the statue you can barely make out some writing. Is says \"BEWERE OF MERCERS PET WUMPUS\"");
		
		
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		if(command.get(0).equalsIgnoreCase("pull")){
			if(command.get(1).equalsIgnoreCase("lever")){
				Item dim = new Ruby(this.exit);
				items.add(dim);
				return "A compartment in the floor opens up and a blood red ruby is revealed.";
			}
			return "You cannot pull that.";
		}
		return "You cannot use that command here.";
	}

}
