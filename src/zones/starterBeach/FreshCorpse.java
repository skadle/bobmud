package zones.starterBeach;

import game.AllItems;
import model.Item;
import model.Living;
import model.livings.Player;

public class FreshCorpse extends Item {

	public FreshCorpse(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	protected void setDefaults() {
		AllItems.add(this);
		name = "A freshly slain corpse";
		desc = "Blood pools around some poor bloke's body, obviously slain very recently.";
		weight = 10000;
		tags.add("corpse");
		tags.add("fresh corpse");
		tags.add("freshly slain corpse");
		tags.add("body");
		tags.add("dead body");
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}


	@Override
	public String execute(Living living) {
		// TODO Auto-generated method stub
		return null;
	}

}
