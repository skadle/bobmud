package zones.starterBeach;

import java.util.ArrayList;

import model.Room;
import model.rooms.NormalRoom;
import zones.general.Jungle;

public class StarterBeach extends NormalRoom {

	boolean alreadyGotPouchFlag = false;
	Room j1 = new Jungle("j1");
	Room j2 = new Jungle("j2");
	Room j3 = new Jungle("j3");
	Room j4 = new Jungle("j4");
	Room j5 = new Jungle("j5");
	Room j6 = new Jungle("j6");
	Room starterHill = new StarterHill("starterHill");

	public StarterBeach(String id) {

		super(id);
		commands.add("search");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

		String waves = "Your feet sink down slightly into the wet sand as small waves lap at the shore.";
		String sun = "The sun beats down viciously upon you, drying the saltwater on your skin.";
		String wind = "A light breeze cools your face and rustles your hair.";
		String cloud = "Clouds pass beneath the sun.";
	}

	@Override
	public void setDefaults() {

		name = "A deserted beach";
		desc = "    You squint and shade your eyes under the brilliant, angry glare\n"
				+ "of the sun. Before you stretches the beach of an unknown island in\n"
				+ "the Caribbean Sea. Bits of driftwood and flotsom as well as coconut\n"
				+ "husks mar the pristine sand of the shore. A dozen yards inland, the\n"
				+ "dense jungle rises up, promising shade as well as dangers untold.";
		nouns.put("beach", desc);
		String sun = "You squint your eyes at the sun. You can't tell if it's rising or falling.";
		nouns.put("sun", sun);
		String wood;
		wood = "    The current has carried floating wood from all over the Atlantic.\n"
				+ "Fallen tree trunks and branches make up most of the wooden mess here.\n"
				+ "Among the driftwood, you notice one oddity - the broken mast of a ship.";
		nouns.put("driftwood", wood);
		nouns.put("flotsam", wood);
		nouns.put("wood", wood);
		String mast;
		mast = "    The top of a broken ship's main mast lays partially on the beach, its\n"
				+ "crow's nest anchored in the sand. The sun-bleached skeletal bones of a man"
				+ "is lashed tightly to the top. Whether the sailor tied himself there during"
				+ "a fould storm or was put there as punishment, none but the gods can say.\n"
				+ "A small leather pouch is tied to his waist. Perhaps you should search it\n"
				+ "to see what this man has left you.";
		nouns.put("mast", mast);
		nouns.put("broken mast", mast);
		nouns.put("crow's nest", mast);
		String skel;
		skel = "A grisly reminder of the dangerous life of a sailor. He has a small belt pouch.";
		nouns.put("skeleton", skel);
		String pouch;
		pouch = "A small pouch is tied to the skeleton's waist. Perhaps you should search it.";
		nouns.put("pouch", pouch);
		nouns.put("belt pouch", pouch);
		nouns.put("leather pouch", pouch);
		nouns.put("small leather pouch", pouch);
	}

	private String searchPouch() {
		if (alreadyGotPouchFlag) {
			return "The pouch has already washed away in the surf.";

		} else {
			String pouch = "The pouch has already washed away in the surf.";
			nouns.put("pouch", pouch);
			nouns.put("belt pouch", pouch);
			nouns.put("leather pouch", pouch);
			nouns.put("small leather pouch", pouch);
			String skel = "A grisly reminder of the dangerous life of a sailor.";
			nouns.put("skeleton", skel);
			String mast;
			mast = "    The top of a broken ship's main mast lays partially on the beach, its\n"
					+ "crow's nest anchored in the sand. The sun-bleached skeletal bones of a man"
					+ "is lashed tightly to the top. Whether the sailor tied himself there during"
					+ "a fould storm or was put there as punishment, none but the gods can say.\n";
			nouns.put("mast", mast);
			nouns.put("broken mast", mast);
			nouns.put("crow's nest", mast);
			BatteredKnife knife = new BatteredKnife();
			this.addItem(knife);
//			GoldCoins gold = new GoldCoins("beachGold");//commented out for testing
//			gold.setName("stuff");
//			gold.setValue(2);
//			this.addItem(gold);
			return "    You reach between the skeleton's ribs and untie the pouch. It falls to the ground\n"
					+ "spilling its contents onto the sand: a battered knife and a pair of crusty gold coins.";
		}
	}

	@Override
	public String useCommand(ArrayList<String> list) {
		// TODO Auto-generated method stub
		if(list.get(0).equals("search")){
			if(list.get(1).equals("pouch")){
				return searchPouch();
			}
			else{
				return "You can not search that";
			}
		}
		return "this should never be returned... \nPlease ask steve for advice on how to solve the problem.";
	}
}
