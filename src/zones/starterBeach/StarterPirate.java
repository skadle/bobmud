package zones.starterBeach;

import game.AllLivings;
import model.Room;
import model.livings.Creature;

public class StarterPirate extends Creature {

	public StarterPirate(String id, Room room) {
		super(id, room);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setDefaults() {
		AllLivings.add(this);
		name = "A dirty pirate searches through the pockets of a dead man.";
		shortName = "dirty pirate";
		desc = "    A disheveled pirate is hunched over the body of a dead man. He wears\n"
				+ "the typical garb of a pirate: striped shirt, ragged pants, layers upon layers\n"
				+ "of unwashed dirt and grim. The dirty pirate eyes you with malice as you approach.";
		hp = 1;
		vision = 0;
		defense = 0;
		attack = 0;
		tags.add(shortName);
		tags.add("pirate");
		tags.add("disheveled pirate");
		tags.add("dirty pirate");
	}

	@Override
	public void chatter() {
		String cmere = "The dirty pirate says, \"Hullo, boy! Have any coin for me?\"";
		String eyes = "The dirty pirate eyes you suspiciously.";
		String fists = "The pirate clenches his fists, readying for a fight.";
		String threaten = "The pirate threatens you, \"Your gold or your life, matey!\"";
		String search = "The pirate rolls the body over, search for any missed coin.";
		String tooth = "The dirty pirate spits out a broken, bloody tooth.";
		// TODO Auto-generated method stub

	}
}
