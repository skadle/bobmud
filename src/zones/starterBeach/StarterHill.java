package zones.starterBeach;

import java.util.ArrayList;

import model.rooms.NormalRoom;

public class StarterHill extends NormalRoom {

	public StarterHill(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void chatter() {

	}

	@Override
	public void setDefaults() {
		name = "A hill overlooking the fabled pirate city of Tortuga";
		desc = "    As you step from the jungle into the brilliant light, you feel the\n"
				+ "welcoming warmth of the sun's light upon your salty skin. You are\n"
				+ "standing atop a tall hill, surrounded below by dense jungle. Below you,\n"
				+ "is the unmistakable town of Tortuga around a bay of crystal blue water.\n"
				+ "Numerous tall ships dot the cove, anchored here and there. Small pinnaces\n"
				+ "ferry the ships' crews and cargo between the town and the ships. Far\n"
				+ "across the bay at the other end of town is a steep, rocky mountain. You\n"
				+ "can barely make out what appears to be a wooden tower atop the peak.\n"
				+ "The jungle trail continues down into the fabled pirate town of Tortuga.";
		nouns.put("hill", desc);
		String trees = "The trees crowd in around you on all sides, looming overhead.";
		nouns.put("trees", trees);
		String tortuga;
		tortuga = "    The pirate haven of Tortuga lies below you along the coast.\n"
				+ "The town proper is surrounded by shantys and back-alleys, and further\n"
				+ "out still, by endless jungle. This haven for pirates and outlaws is\n"
				+ "notorious as a wretched hive of scum and villainy.";
		nouns.put("tortuga", tortuga);
		nouns.put("town", tortuga);
		nouns.put("pirate town", tortuga);
		nouns.put("coastal town", tortuga);
		String mountain;
		mountain = "    Not as much a mountain as simply a very tall and steep hill, but still\n"
				+ "dizzying in its height. Atop the mountain is a wooden lookout tower.";
		nouns.put("mountain", mountain);
		String tower;
		tower = "    The tower, barely visible from this distance, keeps watch for ships of the\n" +
				"Royal Navy, storms, or anything else that might threaten Tortuga.";
		nouns.put("tower", tower);
		nouns.put("wooden tower", tower);
		String jungle = "The jungle lies behind you, as well as all over the island of Tortuga.";
		nouns.put("jungle", jungle);
		String ships = "Tall ships are at anchor in the bay, most flying the black flag.";
		nouns.put("ships", ships);
		String flag = "Pirate ships sometimes fly a black flag, some with the famous Jolly Roger [ :x ]";
		nouns.put("flag", flag);
		String pinnaces = "    Large rowboats, miniscule at this distance but distinct against the\n"
				+ "crystal clear water, ferry crew and cargo between the ships and the docks.";
		nouns.put("pinnaces", pinnaces);
		nouns.put("rowboats", pinnaces);
		nouns.put("large rowboats", pinnaces);
		String water = "The crystal clear water is why pirates love sailing. That and the booty.";
		nouns.put("water", water);
		nouns.put("bay", water);
		nouns.put("booty", "You can almost see it now, all that glittering booty...");

	}

	@Override
	public String useCommand(ArrayList<String> command) {
		// TODO Auto-generated method stub
		return null;
	}
}
