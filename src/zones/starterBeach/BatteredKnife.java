package zones.starterBeach;

import model.items.ModelWeapon;
import model.livings.Player;

public class BatteredKnife extends ModelWeapon {
	public BatteredKnife() {
		super("Battered Knife");
		// TODO Auto-generated constructor stub
	}

	protected void setDefaults() {
		name = "A battered knife";
		desc = "    This short, rusty knife is caked in sea salt. The handle is a strip of\n"
				+ "leather knotted tightly around one end of the blade. It's not fancy, but you\n"
				+ "would do better in a scrap than with your bare hands.";
		weight = 1;
		tags.add(name);
		tags.add("battered knife");
		tags.add("knife");
		tags.add("rusty knife");
	}

	@Override
	public void chatter() {
		// TODO Auto-generated method stub

	}

}
