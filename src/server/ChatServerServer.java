package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ChatServerServer implements Runnable {

	String chat;
	public static final int PORT_NUMBER = 4010;
	ArrayList clientList = new ArrayList();
	ServerSocket ss;

	public static void main(String[] args) {
		ChatServerServer css = new ChatServerServer();
	}

	public ChatServerServer() {
		super();
		try {
			 ss = new ServerSocket(PORT_NUMBER);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//		try {
//			ServerSocket ss = new ServerSocket(PORT_NUMBER);
//			while (true) {
//				Socket socketFromTheClient = ss.accept();
//				OutputStream os = socketFromTheClient.getOutputStream();
//				PrintWriter pw = new PrintWriter(os);
//				addToClientList(pw);
//				Thread t = new Thread(new ServerThread(socketFromTheClient));
//				t.start();
//			}
//		} catch (Exception ex) {
//			System.out.println("exception in client constructor!");
//			ex.printStackTrace();
//		}
	}
	
	public void ConnectTo(){ 
			try {
		Socket socketFromTheClient = ss.accept();
		OutputStream os = socketFromTheClient.getOutputStream();
		PrintWriter pw = new PrintWriter(os);
		addToClientList(pw);
		Thread t = new Thread(new ServerThread(socketFromTheClient));
		t.start();
	
} catch (Exception ex) {
	System.out.println("exception in client constructor!");
	ex.printStackTrace();
}
	}
	public void addToClientList(Object E) {
		clientList.add(E);
	}

	public class ServerThread implements Runnable {
		BufferedReader br;

		public ServerThread(Socket theSocket) {
			try {
				// sock.getPort();
				InputStream is = theSocket.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				br = new BufferedReader(isr);
			} catch (Exception ex) {
				System.out.print("exception in ServerThread");
				ex.printStackTrace();
			}
		}

		public void run() {
			try {
				while ((chat = br.readLine()) != null) {
					sendToClientList(chat);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void sendToClientList(String theChatter) {
		for (int i = 0; i < clientList.size(); i++) {
			try {
				PrintWriter writer = (PrintWriter) clientList.get(i);
				writer.println(theChatter);
				writer.flush();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}
