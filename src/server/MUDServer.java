package server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JOptionPane;

import model.Game;
import model.Living;
import model.PasswordSave;
import model.livings.Player;
public class MUDServer {

//        String chat;
        public static final int PORT_NUMBER = 4007;
        ArrayList clientList = new ArrayList();
        HashMap<String,String> passwordMap = new HashMap<String,String>();
        //passwordMap = doLoadPass(passwordMap);
        public Game theGame = new Game();
        PrintWriter pw;//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int counter =0;
        boolean keepRunningFlag=true;
        HashMap<String,PrintWriter> hm = new HashMap<String,PrintWriter>();
        ChatServerServer clientServer;

        public static void main(String[] args) {
                MUDServer css = new MUDServer();
        }

        public MUDServer() {
                try {
                        ServerSocket ss = new ServerSocket(PORT_NUMBER);
                        //Thread thread
                        clientServer = new ChatServerServer();
                        loadPasswords();
                        while (keepRunningFlag) {
                                Socket socketFromTheClient = ss.accept();
                                OutputStream os = socketFromTheClient.getOutputStream();
                                 pw = new PrintWriter(os);
                                addToClientList(pw);
                                Thread t = new Thread(new ServerThread(socketFromTheClient,pw,this));
                                t.start();
                                if(counter==0){
                                //Game theGame = new Game();
                                counter++;
                                }
                                clientServer.ConnectTo();
                                        //Living liv1 = new Player("1", theGame.getStartingRoom());// PROBLEM SPOT, may need a method to create players otherwise cannot parse
                        //      livings.add(liv1);
                                //      theGame.addLiving(liv1);
                                }
                                
                } catch (Exception ex) {
                        System.out.println("exception in client constructor!");
                        ex.printStackTrace();
                }
        }

        public void addToClientList(Object E) {
                clientList.add(E);
        }

        public class ServerThread implements Runnable {
                BufferedReader br;
                boolean nameSet=false;
                String name = "";
                PrintWriter thePW;
                boolean askForPassword=true;
                MUDServer ms;
                public ServerThread(Socket theSocket,PrintWriter pw,MUDServer ms) {
                        try {
                                // sock.getPort();
                        	this.ms=ms;
                                thePW=pw;
                                InputStream is = theSocket.getInputStream();
                                InputStreamReader isr = new InputStreamReader(is);
                                br = new BufferedReader(isr);
                        } catch (Exception ex) {
                                System.out.print("exception in ServerThread");
                                ex.printStackTrace();
                        }
                }

                public void run() {
                	String chat;
                        
                        try {
                                
                                while ((chat = br.readLine()) != null) {
                                        if(nameSet==false){
                                                //if(hm.containsKey(chat)){
                                                        
                                                //}
                                                name = chat;
                                                nameSet=true;
                                                if(passwordMap.containsKey(name)){
                                                	while(askForPassword==true){
                                                	//	Object[] possibilities = {"ham", "spam", "yam"};
                                               // 		String s = (String)JOptionPane.showInputDialog(
                                                		                 //   null,
                                                		               //     "Complete the sentence:\n"
                                                		             //       + "\"Green eggs and...\"",
                                                		           //         "Customized Dialog",
                                                		         //           JOptionPane.PLAIN_MESSAGE
                                                		        //            ,icon,
                                                		  //                  possibilities,
                                                		      //              "ham"
                                                		         //           );
                                                   		String s =  JOptionPane.showInputDialog(this 
                                             			       ,"Please enter your password for "+name);
                                             			if(passwordMap.get(name).equals(s)){
                                                				askForPassword=false;
                                                			}
                                                			
                                                			
                                                			
                                                		//If a string was returned, say so.
//                                                		if ((s != null) && (s.length() > 0)) {
//                                                		    setLabel("Green eggs and... " + s + "!");
//                                                		    return;
//                                                		}

                                                		//If you're here, the return value was null/empty.
                                          //      		setLabel("Come on, finish the sentence!");
                                                	}
                                                	Living liv = new Player("@#$^@$%&", theGame.getStartingRoom());
                                                	theGame.parse("_load "+ name, "@#$^@$%&", ms);
                                                	hm.put(name,thePW);
                                                	theGame.addLiving(liv);
                                                	sendItToTheGame("look",name,thePW);
                                                }
                                                else{
                                                	
                                                	//Component frame;
													JOptionPane.showMessageDialog(null, "No player by the name " + name + " exists! A new player will be created!");
                                               		String s =  JOptionPane.showInputDialog(this 
                                         			       ,"Please enter new password for "+name);
                                               	passwordMap.put(name, s);
                                               	savePasswords(passwordMap);
                                                hm.put(name, thePW);
                                                Living liv = new Player(name,theGame.getStartingRoom());
                                                theGame.addLiving(liv);
                                                sendItToTheGame("look",name,thePW);
                                        }}
                                        else{
                                        	//System.out.print(chat.substring(0,3));
                                        	if(chat.length()>2&&chat.substring(0,3).equals("ooc")){
                                        		chat = chat.substring(3,chat.length()-1);
                                        		String s = name+": "+chat;
                                        		clientServer.sendToClientList(s);
                                        	}else{
                                        		if(chat.length()>2&&chat.substring(0,4).equals("tell")){
                                        			Scanner s = new Scanner(chat.substring(4,chat.length()-1));
                                        			String string = s.next();
                                        			sendToPlayer(string, chat.substring(string.length(),chat.length()-1));
                                        		}else{
                                        			if(chat.length()>2&&chat.substring(0,3).equals("say")){
                                        		//	ArrayList<String> al = getPlayerIDRoom(name);
                                        	//	for(int i= 0;i<al.size();i++){
                                        		//	sendToPlayer(al.get(i),chat.substring(3,chat.length()-1));
                                        			
                                        		//}
                                        			}
                                        		}
                                        	}
                                        sendItToTheGame(chat,name,thePW);
                                //      sendToClientList(chat);
                                        }
                                }
                        } catch (Exception ex) {
                                ex.printStackTrace();
                        }
                }
        }
        
        public void sendItToTheGame(String chat,String name,PrintWriter pw){
                sendToOneClient(theGame.parse(chat, name, this),pw);
                
        }
        
        public void sendToOneClient(String s,PrintWriter pw){
                PrintWriter writer = pw;
                writer.println(s);
                writer.flush();
        }
        
        public void shutDown(){
                keepRunningFlag=false;
        }

        public void sendToClientList(String theChatter) {
                for (int i = 0; i < clientList.size(); i++) {
                        try {
                                PrintWriter writer = (PrintWriter) clientList.get(i);
                                writer.println(theChatter);
                                writer.flush();
                        } catch (Exception ex) {
                                ex.printStackTrace();
                        }
                }
        }

        public void sendToPlayer(String id, String string) {
        	if(hm.containsKey(id)){
                PrintWriter thePrintWriter = hm.get(id);
                thePrintWriter.println(string);
                thePrintWriter.flush();
        }}
        public void savePasswords(HashMap<String, String> passwords) {
            PasswordSave passwordSave = new PasswordSave(passwords);
            try {
                    FileOutputStream fileOut = new FileOutputStream("passwords.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(passwordSave);
                    out.close();
                    fileOut.close();
                    System.out.println("Passwords saved.");
            } catch (FileNotFoundException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }
    }

    public void loadPasswords() {
            PasswordSave passwordSave = null;
            try {

                    FileInputStream fileIn = new FileInputStream("passwords.ser");
                    ObjectInputStream in = new ObjectInputStream(fileIn);

                    passwordSave = (PasswordSave) in.readObject();
                    in.close();
                    fileIn.close();
                    this.passwordMap=passwordSave.getMap();

                    System.out.println("Password list loaded.");
            } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
            } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
            } catch (IOException ex) {
                    ex.printStackTrace();
            }
    }
        

}