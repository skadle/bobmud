package server;

/*
 * Author: Steve Murphy, Sung Kim
 * used Dr.Mercer code of MultiClientNetworking.zip as a outline thank you
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class chatServerGUI extends JFrame {

	private JTextField text = new JTextField(29);
	private JTextArea messages = new JTextArea();
	private JPanel textArea = new JPanel();
	private String Username;
	private int first = 0;
	private int numOfStrings;
	
	public static final String HOST_NAME = "localhost";

	// For this HOST_NAME, start the server on Lectura
	// public static final String HOST_NAME = "lec.cs.arizona.edu";
	public static final int PORT_NUMBER = 4009;

	private ObjectOutputStream outputToLiasonLoop; // stream to server
	private ObjectInputStream inputFromLiasonLoop; // stream from server
	
	private Vector<String> connectedClients;

	private JButton next = new JButton("Enter");
	private JButton quitButton = new JButton("Quit");

	private JScrollPane scrollPane;

	public static void main(String[] args) {
		chatServerGUI gui = new chatServerGUI();
		gui.setVisible(true);
		gui.setResizable(false);
		gui.startTheReadThread();
	}

	private void startTheReadThread() {
		connectToServer();
		IncomingReader ir = new IncomingReader();
		Thread thread = new Thread(ir);
		thread.start();
	}

//	public chatServerGUI() {
//		this.setLocation(50, 100);
//		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		this.setTitle("username");
//		
//		ButtonListener listener = new ButtonListener();
//		text.addActionListener(listener);
//		
//		this.add(text);
//		this.pack();
//	}
	
	public chatServerGUI() {
		//this.remove(text);
		this.setSize(500, 400);
		this.setLocation(50, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Chat");

		Border border = BorderFactory.createLineBorder(Color.BLACK);
		ButtonListener listener = new ButtonListener();
		text.addActionListener(listener);
		quitButton.addActionListener(listener);

		text.setBorder(border);
		textArea.add(new Label("Put:"));
		textArea.add(text);
		textArea.add(quitButton);
		messages.setLineWrap(true);
		messages.setWrapStyleWord(true);
		messages.setEditable(false);
		messages.setText("Is this your first login?");
		scrollPane = new JScrollPane(messages);
		scrollPane.setPreferredSize(new Dimension(480, 330));

		textArea.add(scrollPane);
		this.add(textArea);
	}

	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			Object eventSource = event.getSource();
			try {
				if (eventSource == text) {
					if(text.getText().equalsIgnoreCase("yes") || text.getText().equalsIgnoreCase("y")){
						messages.append("what is your name?");
					} else if (text.getText().length() > 0){
						outputToLiasonLoop.writeObject("put " + Username + ": " + text.getText());
					}
					text.setText("");
				}
				if (eventSource == quitButton) {
					outputToLiasonLoop.writeObject("quit");
					System.exit(0);
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	public void connectToServer() {
		Socket sock = null;
		try {
			// connect to the server
			sock = new Socket(HOST_NAME, PORT_NUMBER);
		} catch (Exception e) {
			System.out.println("Client was unable to connect to server");
			e.printStackTrace();
		}
		try {
			outputToLiasonLoop = new ObjectOutputStream(sock.getOutputStream());
			inputFromLiasonLoop = new ObjectInputStream(sock.getInputStream());
			connectedClients = (Vector<String>) inputFromLiasonLoop.readObject();
			numOfStrings = connectedClients.size();
			
		} catch (Exception e) {
			System.out.println("Unable to obtain Input/Output streams from Socket");
			e.printStackTrace();
		}
	}

	class IncomingReader implements Runnable {

		@SuppressWarnings("unchecked")
		public void run() {
			while (true) {
				Vector<String> clientsConnected = null;
				try {
					clientsConnected = (Vector<String>) inputFromLiasonLoop.readObject();
					messages.setText("");
					for (int i = numOfStrings; i < clientsConnected.size(); i++)
						messages.append(clientsConnected.get(i) + "\n");
				} catch (ClassNotFoundException cnfe) {
					cnfe.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
}
