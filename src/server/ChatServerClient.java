package server;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ChatServerClient extends JFrame {
	JTextArea inputArea;
	JTextField outputArea;
	String chat;
	Socket sock;
	private int nameFlag = 0;
	static JTextField putField = new JTextField(20);
	static JTextArea textArea = new JTextArea();
	String name = "Anonymous";
	BufferedReader reader;
	PrintWriter writer;
	public static final String HOST_NAME = "localhost";

	// For this HOST_NAME, start the server on Lectura
	// public static final String HOST_NAME = "lec.cs.arizona.edu";
	public static final int PORT_NUMBER = 4010;

	public ChatServerClient(String name) {
		this.name = name;
		setSize(500, 400);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel frame = new JPanel();
		putField.setText("Replace me with your name");
	    ButtonListener listener = new ButtonListener();
		putField.addActionListener(listener);
		frame.add(putField);
		textArea.setPreferredSize(new Dimension(480, 330));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		frame.add(textArea);
		this.add(frame);
		frame.setSize(650, 500);
		frame.setVisible(true);
		setUpNetworking();
	
		Thread readerThread = new Thread(new InputRecieved());
		readerThread.start();
	}

	class InputRecieved implements Runnable {
		public void run() {
			try {
				while ((chat = reader.readLine()) != null) {
				//	if(nameFlag==1){
					textArea.append(chat + "\n");
				//}
					}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	private void setUpNetworking() {
		try {
			sock = new Socket(HOST_NAME, PORT_NUMBER);
			InputStreamReader streamReader = new InputStreamReader(
					sock.getInputStream());
			reader = new BufferedReader(streamReader);
			writer = new PrintWriter(sock.getOutputStream());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	 private class ButtonListener implements ActionListener {

		    @Override
		    public void actionPerformed(ActionEvent event) {
		      try {
					if (nameFlag == 0) {
						//if (putField.getText().length() > 0) {
						//	name = putField.getText();
						//}
							writer.println(name+ " has joined the server!");
							writer.flush();
							putField.setText("");
						nameFlag = 1;
					} else {
						writer.println(name + ": " + putField.getText());
						writer.flush();
						putField.setText("");
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		    }
		  }


//	public static void main(String[] args) {
//		ChatServerClient frame = new ChatServerClient();
//		frame.setVisible(true);	
//	}

}
