package server;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class MUDClient extends JFrame {
	JTextArea inputArea;
	JTextField outputArea;
	String chat;
	Socket sock;
	private int nameFlag = 0;
	static JTextField putField = new JTextField(20);
	static JTextArea textArea = new JTextArea();
	String name = "Anonymous";
	BufferedReader reader;
	PrintWriter writer;
	JScrollPane scrollPane;
	JScrollBar theBar;
	String connectTo = "localhost";
	public static final String HOST_NAME = "localhost";


	// For this HOST_NAME, start the server on Lectura
	// public static final String HOST_NAME = "lec.cs.arizona.edu";
	public static final int PORT_NUMBER = 4007;

	public MUDClient() {
		chooseConnection();
		setSize(500, 400);
		setResizable(false);
		  setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);                              addWindowListener(new java.awt.event.WindowAdapter() {
              public void windowClosing(java.awt.event.WindowEvent e) {
                      System.out.println("Closing.");
                      saveGame();
                      System.exit(0);
              }
      });
		JPanel frame = new JPanel();
		putField.setText("Replace me with your name");
	    ButtonListener listener = new ButtonListener();
		putField.addActionListener(listener);
		frame.add(putField);
		//textArea.setPreferredSize(new Dimension(480, 330));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		//frame.add(textArea);
		scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(480,330));
		frame.add(scrollPane);
		this.add(frame);
		//frame.setSize(500, 400);
		frame.setVisible(true);
		setUpNetworking();
		
//		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
//	        public void adjustmentValueChanged(AdjustmentEvent e) {  
//	            e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
//	        }
//	    });
		Thread readerThread = new Thread(new InputRecieved());
		readerThread.start();
	}
	private void chooseConnection() {
		if (JOptionPane.showConfirmDialog(null, "Connect to Server? (Select 'No' for local game)", "Choose Connection",
		        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
		   connectTo="192.168.1.1";
		} else {
		    connectTo = "localhost";
		}
		
	}
	// public void adjustmentValueChanged(AdjustmentEvent e) {  
     //    e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
    // }
//	
//	public void moveScrollToBottom(){
//		textArea.setCaretPosition(textArea.getDocument().getLength());
//	}
private void saveGame(){
	writer.println("save");
	writer.flush();
}
	class InputRecieved implements Runnable {
		public void run() {
			try {
				while ((chat = reader.readLine()) != null) {
					if(nameFlag==1){
					textArea.append(chat + "\n");
				}
					textArea.setCaretPosition(textArea.getDocument().getLength());
					//theBar = scrollPane.getVerticalScrollBar();
					//theBar.setValue(theBar.getMaximum());	
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		//	scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
		//	textArea.setCaretPosition(scrollPane.getDocument().getLength());
		}
	}
	private void setUpNetworking() {
		try {
			sock = new Socket(HOST_NAME, PORT_NUMBER);
			InputStreamReader streamReader = new InputStreamReader(
					sock.getInputStream());
			reader = new BufferedReader(streamReader);
			writer = new PrintWriter(sock.getOutputStream());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	 private class ButtonListener implements ActionListener {

		    @Override
		    public void actionPerformed(ActionEvent event) {
		      try {
					if (nameFlag == 0) {
						if (putField.getText().length() > 0) {
							name = putField.getText();
						}
						//	writer.println(name+ " has joined the server!");
						//	writer.flush();
						nameFlag = 1;
						writer.println(name);
						writer.flush();
						//writer.println("look");////FUCK
						//writer.flush();
						//writer.println("look");
						//writer.flush();
						putField.setText("");
						ChatServerClient client = new ChatServerClient(name);
						client.setVisible(true);
					} else {
						//writer.println(name + ": " + putField.getText());
						writer.println(putField.getText());
						writer.flush();
						putField.setText("");
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		    }
		  }


	public static void main(String[] args) {
		MUDClient frame = new MUDClient();
		frame.setVisible(true);	
	}

}
