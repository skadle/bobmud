package game;

import java.io.Serializable;
import java.util.HashMap;

import model.Item;

public class AllItems implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static HashMap<String, Item> items = new HashMap<String, Item>();

	public AllItems() {
	}

	public static void add(Item item) {
		items.put(item.getID(), item);
	}

	public static HashMap<String, Item> getItems() {
		return items;
	}
}
