package game;

import java.io.Serializable;
import java.util.HashMap;

import model.Living;

public class AllLivings implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static HashMap<String, Living> livings = new HashMap<String, Living>();

	public AllLivings() {
	}

	public static void add(Living living) {
		livings.put(living.getID(), living);
	}

	public Living get(String str) {
		return livings.get(str);
	}

	public static HashMap<String, Living> getLivings() {
		return livings;
	}
}
