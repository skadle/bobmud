package game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.Room;
import zones.burningForest.BurningCave;
import zones.burningForest.BurningForestEntrance;
import zones.burningForest.DeeperBurningForest;
import zones.general.Jungle;
import zones.mountain.MountainPath;
import zones.sewers.Celler;
import zones.starterBeach.EdgeOfTheJungle;
import zones.starterBeach.StarterBeach;
import zones.starterBeach.StarterHill;
import zones.tortuga.Bar;
import zones.tortuga.Docks;
import zones.tortuga.Dutchman;
import zones.tortuga.Empire;
import zones.tortuga.ErrorRoom;
import zones.tortuga.Graveyard;
import zones.tortuga.Manor;
import zones.tortuga.Mission;
import zones.tortuga.Pier;
import zones.tortuga.TortugaBurnedBuilding;
import zones.tortuga.TortugaCenter;
import zones.tortuga.TortugaEast;
import zones.tortuga.TortugaNorth;
import zones.tortuga.TortugaSouth;
import zones.tortuga.TortugaWest;
import zones.tortuga.TradingPost;
import zones.tortuga.Warehouse;

public class AllRooms implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	static HashMap<String, Room> rooms = new HashMap<String, Room>();
	private List<Room> rooms;

	public AllRooms() {
		rooms = new ArrayList<Room>();
		
		Room starterBeach = new StarterBeach("starterBeach");
		Room j1 = new Jungle("j1");
		Room j2 = new Jungle("j2");
		Room j3 = new Jungle("j3");
		Room j4 = new Jungle("j4");
		Room j5 = new Jungle("j5");
		Room j6 = new EdgeOfTheJungle("j6");
		Room starterHill = new StarterHill("starterHill");
		
		Room tortugaW = new TortugaWest("tw");
		Room tortugaS = new TortugaSouth("ts");
		Room tortugaC = new TortugaCenter("tc");
		Room tortugaE = new TortugaEast("te");
		Room tortugaN = new TortugaNorth("tn");
		Room mission = new Mission("mission");
		Room graveyard = new Graveyard("graveyard");
		Room manor = new Manor("manor");
		Room bar = new Bar("bar");
		Room shop = new TradingPost("shop");
		Room warehouse = new Warehouse("warehouse");
		Room docks1 = new Docks("d1");
		Room docks2 = new Docks("d2");
		Room docks3 = new Docks("d3");
		Room pier = new Pier("pier");
		Room dutchman = new Dutchman("dutchman");
		Room empire = new Empire("empire");
		Room tortugaBurn = new TortugaBurnedBuilding("tbb");

		Room j11 = new Jungle("j11");
		Room j12 = new Jungle("j12");
		Room burning1 = new BurningForestEntrance("burning1");
		Room burning2 = new DeeperBurningForest("burning2");
		Room burningCave = new BurningCave("burningCave");

		Room j21 = new Jungle("j21");
		Room j22 = new Jungle("j22");
		Room j23 = new Jungle("j23");
		Room mountainPath = new MountainPath("mPath");

		Room celler = new Celler("celler");

		starterBeach.addExit("jungle", j1);
		j1.addExit("beach", starterBeach);
		j1.addExit("northeast", j2);
		j2.addExit("east", j3);
		j2.addExit("southeast", j4);
		j3.addExit("west", j2);
		j3.addExit("south", j5);
		j4.addExit("northwest", j2);
		j4.addExit("east", j5);
		j5.addExit("north", j3);
		j5.addExit("northeast", j6);
		j6.addExit("southwest", j5);
		j6.addExit("east", starterHill);
		starterHill.addExit("west", j6);
		starterHill.addExit("down", tortugaS);

		tortugaC.addExit("south", tortugaS);
		tortugaC.addExit("east", tortugaE);
		tortugaC.addExit("north", tortugaN);
		tortugaN.addExit("south", tortugaC);
		tortugaN.addExit("north", j11);
		tortugaE.addExit("west", tortugaC);
		tortugaE.addExit("north", manor);
		tortugaE.addExit("south", bar);
		tortugaE.addExit("east", docks2);
		shop.addExit("northwest", tortugaE);
		shop.addExit("east", warehouse);
		warehouse.addExit("west", shop);
		warehouse.addExit("east", docks3);
		tortugaE.addExit("southeast", shop);
		bar.addExit("north", tortugaE);
		tortugaS.addExit("north", tortugaC);
		tortugaS.addExit("west", tortugaW);
		tortugaS.addExit("south", mission);
		tortugaW.addExit("east", tortugaS);
		mission.addExit("north", tortugaS);
		mission.addExit("east", graveyard);
		graveyard.addExit("west", mission);
		manor.addExit("south", tortugaE);
		docks1.addExit("south", docks2);
		docks1.addExit("west", tortugaBurn);
		docks1.addExit("north", j21);
		docks1.addExit("east", pier);
		pier.addExit("west", docks1);
		docks2.addExit("north", docks1);
		docks2.addExit("south", docks3);
		docks2.addExit("west", tortugaE);
		docks2.addExit("east", dutchman);
		dutchman.addExit("west", docks2);
		docks3.addExit("west", warehouse);
		docks3.addExit("north", docks2);
		docks3.addExit("east", empire);
		empire.addExit("west", docks3);
		tortugaBurn.addExit("east", docks1);
		tortugaBurn.addExit("down", celler);
		celler.addExit("up", tortugaBurn);

		j11.addExit("south", tortugaN);
		j11.addExit("northwest", j12);
		j12.addExit("southeast", j11);
		j12.addExit("west", burning1);
		burning1.addExit("east", j12);
		burning1.addExit("deeper", burning2);
		burning2.addExit("out", burning1);
		burning2.addExit("cave", burning1);
		burningCave.addExit("out", burning2);

		j21.addExit("south", docks1);
		j21.addExit("west", j22);
		j21.addExit("north", j23);
		j22.addExit("east", j21);
		j23.addExit("south", j21);
		j23.addExit("northeast", mountainPath);
		mountainPath.addExit("southwest", j23);

		// starterBeach j1 - j6 starterHill tortugaW tortugaS tortugaC tortugaE tortugaN
		// mission graveyard manor bar shop warehouse docks1 -3 pier dutchman empire tortugaBurn
		// j11 j12 burning1 burning2 burningCave j21 j22 j23 mountainPath celler
		rooms.add(starterBeach);
		rooms.add(j1);
		rooms.add(j2);
		rooms.add(j3);
		rooms.add(j4);
		rooms.add(j5);
		rooms.add(j6);
		rooms.add(starterHill);
		rooms.add(tortugaW);
		rooms.add(tortugaS);
		rooms.add(tortugaC);
		rooms.add(tortugaE);
		rooms.add(tortugaN);
		rooms.add(mission);
		rooms.add(graveyard);
		rooms.add(manor);
		rooms.add(bar);
		rooms.add(shop);
		rooms.add(warehouse);
		rooms.add(docks1);
		rooms.add(docks2);
		rooms.add(docks3);
		rooms.add(pier);
		rooms.add(dutchman);
		rooms.add(empire);
		rooms.add(tortugaBurn);
		rooms.add(j11);
		rooms.add(j12);
		rooms.add(burning1);
		rooms.add(burning2);
		rooms.add(burningCave);
		rooms.add(j21);
		rooms.add(j22);
		rooms.add(j23);
		rooms.add(mountainPath);
		rooms.add(celler);
	}

	public void add(Room room) {
//		rooms.put(room.getName(), room);
		rooms.add(room);
	}

//	public  Room getStart() {
//		return rooms.get("starterBeach");
//	}

	public List<Room> getRooms() {
//		ArrayList<Room> r = new ArrayList<Room>();
		
//		r = (ArrayList<Room>) rooms.values();
		return rooms;
	}

//	public static Room get(String string) {
//		if (rooms.containsKey(string))
//			return rooms.get(string);
//
//		Room error = new ErrorRoom("error");
//		error.addExit("tortuga", AllRooms.getStart());
//
//		return rooms.get("error");
//	}
}
