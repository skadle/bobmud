package tests;

import static org.junit.Assert.assertEquals;
import model.Game;
import model.Item;
import model.Room;
import model.items.Stone;
import model.items.Torch;
import model.livings.Player;

import org.junit.Test;

import zones.general.TestRoom;

public class GameTest {

	@Test
	public void parseTest(){
		Game game = new Game();
		
		Room room = new TestRoom("1");
		Room room2 = new TestRoom("2");
		Room room3 = new TestRoom("3");
		Room room4 = new TestRoom("4");
		Room room5 = new TestRoom("5");
		Room room6 = new TestRoom("6");
		Room room7 = new TestRoom("7");
		Room room8 = new TestRoom("8");
		
		room.addExit("north", room2);
		room.addExit("south", room4);
		room.addExit("east", room6);
		
		room2.addExit("north", room3);
		room2.addExit("south", room);
		room2.addExit("east", room5);
		
		room3.addExit("south", room2);
		
		room4.addExit("east", room8);
		
		room5.addExit("west", room2);
		
		room6.addExit("east", room7);
		room6.addExit("west", room);
		
		room7.addExit("west", room6);
		
		room8.addExit("west", room4);
		
		Player p0 = new Player("0", room);
		Player p1 = new Player("1", room);
		Player p2 = new Player("2", room);
		
		game.addLiving(p0);
		game.addLiving(p1);
		game.addLiving(p2);
		
		Item stuff = new Stone("stuff");
		Item otherStuff = new Torch("other stuff");
		
		assertEquals(game.parse("move north", "0", null), "the player has moved to location north");
		assertEquals(game.parse("go north", "1", null), "the player has moved to location north");
		assertEquals(game.parse("north", "2", null), "the player has moved to location north");
		assertEquals(game.parse("n", "0", null), "the player has moved to location north");
		
		assertEquals(game.parse("move south", "1", null), "the player has moved to location south");
		assertEquals(game.parse("go south", "0", null), "the player has moved to location south");
		assertEquals(game.parse("south", "2", null), "the player has moved to location south");
		assertEquals(game.parse("s", "2", null), "the player has moved to location south");
		
		assertEquals(game.parse("move east", "0", null), "the player has moved to location east");
		assertEquals(game.parse("go east", "1", null), "the player has moved to location east");
		assertEquals(game.parse("east", "2", null), "the player has moved to location east");
		assertEquals(game.parse("e", "1", null), "the player has moved to location east");
		
		assertEquals(game.parse("move west", "0", null), "the player has moved to location west");
		assertEquals(game.parse("go west", "1", null), "the player has moved to location west");
		assertEquals(game.parse("west", "2", null), "the player has moved to location west");
		assertEquals(game.parse("w", "1", null), "the player has moved to location west");
		
		room.addItem(stuff);
		room2.addItem(stuff);

		assertEquals(game.parse("take stuff", "1", null), "the player has taken the item stuff");
		assertEquals(game.parse("get stuff", "0", null), "the player has taken the item stuff");
		
		room.addItem(otherStuff);
		room2.addItem(otherStuff);
		
		assertEquals(game.parse("take other stuff", "1", null), "the player has taken the item other stuff");
		assertEquals(game.parse("get other stuff", "0", null), "the player has taken the item other stuff");
		
		p1.drop(stuff.getID());
		p0.drop(otherStuff.getID());
		
		assertEquals(game.parse("use stuff", "1", null), "Such item does not exist in your inventory");
		assertEquals(game.parse("use other stuff", "0", null), "Such item does not exist in your inventory");
	}
}
