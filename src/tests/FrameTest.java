package tests;

import model.Item;
import model.Living;
import model.Room;
import model.items.FBsword;
import model.livings.Orc;
import model.livings.Skeleton;
import model.livings.Vendor;

import org.junit.Test;

import zones.general.TestRoom;

public class FrameTest {

	@Test
	public void testRoomWithOneExit() {
		Room room = new TestRoom("room1");
		Room room2 = new TestRoom("room2");

		System.out.println(room.toString());

		room.addExit("west", room2);
		System.out.println(room.toString());

		room.addExit("east", room2);
		System.out.println(room.toString());

		room.addExit("up", room2);
		System.out.println(room.toString());

		Vendor vendor = new Vendor("vendor", room);
		System.out.println(vendor.listOfItems());
	}

	@Test
	public void testRoomWithExitsAndLivings() {
		Room room = new TestRoom("room1");
		Room room2 = new TestRoom("room2");
		room.addExit("driver door", room2);
		room.addExit("passenger side", room2);
		Living mob1 = new Skeleton(room);
		mob1.setName("An scarry skeleton stands here.");
		room.addLiving(mob1);
		System.out.println(room.toString());
	}

	@Test
	public void testRoomWithExitsAndLivingsAndItems() {
		Room room = new TestRoom("room1");
		Room room2 = new TestRoom("room2");
		room.addExit("nose", room2);
		room.addExit("ear", room2);
		Living joeSmith = new Orc(room);
		joeSmith.setName("Joe Smith gives you the stink-eye.");
		joeSmith.setShortName("Joe Smith");
		room.addLiving(joeSmith);
		Item woodenSword = new FBsword("FBsword");
		woodenSword.setName("A wooden sword");
		room.addItem(woodenSword);
		room.addItem(woodenSword);
		room.addItem(woodenSword);
		room.removeItem(woodenSword);
		System.out.println(room.toString());
	}

}
