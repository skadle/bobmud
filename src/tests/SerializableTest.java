package tests;

import model.Room;
import model.livings.Player;

import org.junit.Test;

import zones.general.TestRoom;

public class SerializableTest {

	@Test
	public void test() {
		Room room = new TestRoom("1");
		Player kyle = new Player("Kyle", room);
		// AllRooms allRooms = new AllRooms();
		room.addLiving(kyle);
		kyle.setName("Kyle");
		kyle.setShortName("Player Kyle");
		kyle.setDesc("Kyle is a player, standing in the room.");
		kyle.doSave();
		kyle.doLoad("Kyle");
	}

}
