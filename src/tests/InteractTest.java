package tests;

import static org.junit.Assert.assertEquals;
import model.Item;
import model.Room;
import model.items.Corpse;
import model.items.Stone;
import model.livings.Player;
import model.livings.Vendor;

import org.junit.Test;

import zones.general.TestRoom;


public class InteractTest {

	@Test
	public void interactTest(){
		Room room = new TestRoom("1");
		Vendor vend = new Vendor("vend", room);
		Player p0 = new Player("p0", room);
		Item stuff = new Stone("stone");
		
		assertEquals(vend.sellToPlayer(p0, stuff), "You have purchased stone from the vendor");
		assertEquals(p0.inventory(), "Your inventory contains:\nstone\n");
		assertEquals(vend.buyFromPlayer(p0, stuff.getID()),"You have sold stone to the vendor");
		assertEquals(p0.inventory(), "you do not have any items in your inventory");
		assertEquals(vend.listOfItems(), "I have :\nbow\nchest\nhelmet\ngun\nshield\nshoe\nspear\nsword\nhealth potion\nmana potion\n");
		
		Corpse corp = new Corpse("test");
		
		assertEquals(corp.execute(p0), "You have looted stone from this corpse");
		assertEquals(p0.inventory(), "Your inventory contains:\nstone\n");
	}
}
