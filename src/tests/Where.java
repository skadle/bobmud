package tests;

import java.util.ArrayList;
import java.util.List;

import model.Item;
import model.Living;
import model.Room;
import model.items.ApolloBow;
import model.items.AttackBoots;
import model.items.BeginnerBow;
import model.items.BeginnerChest;
import model.items.BeginnerGun;
import model.items.BeginnerHelmet;
import model.items.BeginnerShield;
import model.items.BeginnerShoe;
import model.items.BeginnerSpear;
import model.items.BeginnerSword;
import model.items.CaptainJackGun;
import model.items.Corpse;
import model.items.DefenseBoots;
import model.items.FBsword;
import model.items.GladiatorHelmet;
import model.items.GoldPouch;
import model.items.IntermediateBow;
import model.items.IntermediateChest;
import model.items.IntermediateGun;
import model.items.IntermediateHelmet;
import model.items.IntermediateShield;
import model.items.IntermediateShoe;
import model.items.IntermediateSpear;
import model.items.IntermediateSword;
import model.items.Landuin;
import model.items.MajorHealthPotion;
import model.items.MajorManaPotion;
import model.items.MinorHealthPotion;
import model.items.MinorManaPotion;
import model.items.Stone;
import model.items.Torch;
import model.items.WarmongerChest;
import model.items.WoodStick;
import model.items.ZinXaoSpear;
import model.livings.CaptainOfPirate;
import model.livings.Chicken;
import model.livings.DavyJones;
import model.livings.FilthyPirate;
import model.livings.Kraken;
import model.livings.NavyCaptain;
import model.livings.NavySoldier;
import model.livings.Orc;
import model.livings.Pig;
import model.livings.Skeleton;
import model.livings.Zombie;
import zones.burningForest.BurningCave;
import zones.burningForest.BurningForestEntrance;
import zones.burningForest.DeeperBurningForest;
import zones.general.Jungle;
import zones.mountain.MountainPath;
import zones.sewers.Cellar;
import zones.sewers.ClockTower;
import zones.sewers.TowerBasement;
import zones.starterBeach.EdgeOfTheJungle;
import zones.starterBeach.StarterBeach;
import zones.starterBeach.StarterHill;
import zones.tortuga.Bar;
import zones.tortuga.Docks;
import zones.tortuga.Dutchman;
import zones.tortuga.Empire;
import zones.tortuga.Graveyard;
import zones.tortuga.Manor;
import zones.tortuga.Mission;
import zones.tortuga.Pier;
import zones.tortuga.TortugaBurnedBuilding;
import zones.tortuga.TortugaCenter;
import zones.tortuga.TortugaEast;
import zones.tortuga.TortugaNorth;
import zones.tortuga.TortugaSouth;
import zones.tortuga.TortugaWest;
import zones.tortuga.TradingPost;
import zones.tortuga.Warehouse;

public class Where {

	private List<Room> rooms;
	
	public Where(){
		Item mBow = new ApolloBow();
		Item aBoot	= new AttackBoots();
		Item bow = new BeginnerBow();
		Item chest = new BeginnerChest();
		Item gun = new BeginnerGun();
		Item helm = new BeginnerHelmet();
		Item shield = new BeginnerShield();
		Item shoe = new BeginnerShoe();
		Item spear = new BeginnerSpear();
		Item sword = new BeginnerSword();
		Item mGun = new CaptainJackGun();
		Item dBoot = new DefenseBoots();
		Item mSword = new FBsword();
		Item mHelm = new GladiatorHelmet();
		Item money = new GoldPouch();
		Item iBow = new IntermediateBow();
		Item iChest = new IntermediateChest();
		Item iGun = new IntermediateGun();
		Item iHelm = new IntermediateHelmet();
		Item iShield = new IntermediateShield();
		Item iShoe = new IntermediateShoe();
		Item iSpear = new IntermediateSpear();
		Item iSword = new IntermediateSword();
		Item mShield = new Landuin();
		Item minorHealth = new MinorHealthPotion();
		Item majorHealth = new MajorHealthPotion();
		Item MinorMana = new MinorManaPotion();
		Item MajorMana = new MajorManaPotion();
		Item stone = new Stone();
		Item torch = new Torch();
		Item mChest = new WarmongerChest();
		Item stick = new WoodStick();
		Item mSpear = new ZinXaoSpear();
		Item corp = new Corpse();
		
		Living pCaptain = new CaptainOfPirate(null);
		Living chicken = new Chicken(null);
		Living davy = new DavyJones(null);
		Living pirate = new FilthyPirate(null);
		Living kraken = new Kraken(null);
		Living nCaptain = new NavyCaptain(null);
		Living navy = new NavySoldier(null);
		Living orc = new Orc(null);
		Living pig = new Pig(null);
		Living skel = new Skeleton(null);
		Living Zomb = new Zombie(null);
		
		rooms = new ArrayList<Room>();

		Room starterBeach = new StarterBeach("starterBeach");
		Room j1 = new Jungle("j1");
		Room j2 = new Jungle("j2");
		Room j3 = new Jungle("j3");
		Room j4 = new Jungle("j4");
		Room j5 = new Jungle("j5");
		Room j6 = new EdgeOfTheJungle("j6");
		Room starterHill = new StarterHill("starterHill");

		Room tortugaW = new TortugaWest("tw");
		Room tortugaS = new TortugaSouth("ts");
		Room tortugaC = new TortugaCenter("tc");
		Room tortugaE = new TortugaEast("te");
		Room tortugaN = new TortugaNorth("tn");
		Room mission = new Mission("mission");
		Room graveyard = new Graveyard("graveyard");
		Room manor = new Manor("manor");
		Room bar = new Bar("bar");
		Room shop = new TradingPost("shop");
		Room warehouse = new Warehouse("warehouse");
		Room docks1 = new Docks("d1");
		Room docks2 = new Docks("d2");
		Room docks3 = new Docks("d3");
		Room pier = new Pier("pier");
		Room dutchman = new Dutchman("dutchman");
		Room empire = new Empire("empire");
		Room tortugaBurn = new TortugaBurnedBuilding("tbb");

		Room clockTower = new ClockTower("clockTower");
		Room towerBasement = new TowerBasement("towerBasement");

		Room j11 = new Jungle("j11");
		Room j12 = new Jungle("j12");
		Room burning1 = new BurningForestEntrance("burning1");
		Room burning2 = new DeeperBurningForest("burning2");
		Room burningCave = new BurningCave("burningCave");

		Room j21 = new Jungle("j21");
		Room j22 = new Jungle("j22");
		Room j23 = new Jungle("j23");
		Room mountainPath = new MountainPath("mPath");

		Room cellar = new Cellar("cellar");

		starterBeach.addExit("jungle", j1);
		j1.addExit("beach", starterBeach);
		j1.addExit("northeast", j2);
		j2.addExit("east", j3);
		j2.addExit("southeast", j4);
		j3.addExit("west", j2);
		j3.addExit("south", j5);
		j4.addExit("northwest", j2);
		j4.addExit("east", j5);
		j5.addExit("north", j3);
		j5.addExit("northeast", j6);
		j6.addExit("southwest", j5);
		j6.addExit("east", starterHill);
		starterHill.addExit("west", j6);
		starterHill.addExit("down", tortugaS);

		tortugaC.addExit("south", tortugaS);
		tortugaC.addExit("east", tortugaE);
		tortugaC.addExit("north", tortugaN);
		tortugaN.addExit("south", tortugaC);
		tortugaN.addExit("north", j11);
		tortugaE.addExit("west", tortugaC);
		tortugaE.addExit("north", manor);
		tortugaE.addExit("south", bar);
		tortugaE.addExit("east", docks2);
		tortugaE.addExit("up", clockTower);
		shop.addExit("northwest", tortugaE);
		shop.addExit("east", warehouse);
		warehouse.addExit("west", shop);
		warehouse.addExit("east", docks3);
		tortugaE.addExit("southeast", shop);
		bar.addExit("north", tortugaE);
		tortugaS.addExit("north", tortugaC);
		tortugaS.addExit("west", tortugaW);
		tortugaS.addExit("south", mission);
		tortugaW.addExit("east", tortugaS);
		mission.addExit("north", tortugaS);
		mission.addExit("east", graveyard);
		graveyard.addExit("west", mission);
		manor.addExit("south", tortugaE);
		docks1.addExit("south", docks2);
		docks1.addExit("west", tortugaBurn);
		docks1.addExit("north", j21);
		docks1.addExit("east", pier);
		pier.addExit("west", docks1);
		docks2.addExit("north", docks1);
		docks2.addExit("south", docks3);
		docks2.addExit("west", tortugaE);
		docks2.addExit("east", dutchman);
		dutchman.addExit("west", docks2);
		docks3.addExit("west", warehouse);
		docks3.addExit("north", docks2);
		docks3.addExit("east", empire);
		empire.addExit("west", docks3);
		tortugaBurn.addExit("east", docks1);
		tortugaBurn.addExit("down", cellar);
		
		
		cellar.addExit("up", tortugaBurn);

		j11.addExit("south", tortugaN);
		j11.addExit("northwest", j12);
		j12.addExit("southeast", j11);
		j12.addExit("west", burning1);
		burning1.addExit("east", j12);
		burning1.addExit("deeper", burning2);
		burning2.addExit("out", burning1);
		burning2.addExit("cave", burningCave);
		burningCave.addExit("out", burning2);

		j21.addExit("south", docks1);
		j21.addExit("west", j22);
		j21.addExit("north", j23);
		j22.addExit("east", j21);
		j23.addExit("south", j21);
		j23.addExit("northeast", mountainPath);
		mountainPath.addExit("southwest", j23);

		// starterBeach j1 - j6 starterHill tortugaW tortugaS tortugaC tortugaE
		// tortugaN
		// mission graveyard manor bar shop warehouse docks1 -3 pier dutchman
		// empire tortugaBurn
		// j11 j12 burning1 burning2 burningCave j21 j22 j23 mountainPath cellar
		rooms.add(starterBeach);
		rooms.add(j1);
		rooms.add(j2);
		rooms.add(j3);
		rooms.add(j4);
		rooms.add(j5);
		rooms.add(j6);
		rooms.add(starterHill);
		rooms.add(tortugaW);
		rooms.add(tortugaS);
		rooms.add(tortugaC);
		rooms.add(tortugaE);
		rooms.add(tortugaN);
		rooms.add(mission);
		rooms.add(graveyard);
		rooms.add(manor);
		rooms.add(bar);
		rooms.add(shop);
		rooms.add(warehouse);
		rooms.add(docks1);
		rooms.add(docks2);
		rooms.add(docks3);
		rooms.add(pier);
		rooms.add(dutchman);
		rooms.add(empire);
		rooms.add(tortugaBurn);
		rooms.add(j11);
		rooms.add(j12);
		rooms.add(burning1);
		rooms.add(burning2);
		rooms.add(burningCave);
		rooms.add(j21);
		rooms.add(j22);
		rooms.add(j23);
		rooms.add(mountainPath);
		rooms.add(cellar);

		starterBeach.addItem(sword);
		starterBeach.addItem(stone);
		starterBeach.addItem(stone);
		starterBeach.addItem(stone);
		starterBeach.addItem(stick);
		starterBeach.addItem(stick);
		starterBeach.addLiving(kraken);
		starterBeach.addItem(minorHealth);
		
		j1.addLiving(chicken);
		j1.addLiving(chicken);
		j1.addLiving(pig);
		j1.addItem(corp);
		
		j2.addItem(spear);
		j2.addLiving(pig);
		j2.addLiving(chicken);
		j2.addLiving(pig);
		j2.addLiving(chicken);
		j2.addLiving(chicken);
		j2.addItem(minorHealth);
		
		j3.addLiving(orc);
		j3.addLiving(pig);
		j3.addItem(stick);
		
		j4.addLiving(skel);
		j4.addLiving(chicken);
		j4.addLiving(chicken);
		j4.addItem(stone);
		j4.addItem(minorHealth);
		
		j5.addLiving(Zomb);
		j5.addItem(shoe);
		j5.addLiving(pig);
		j5.addItem(corp);
		
		j6.addLiving(pirate);
		j6.addItem(chest);
		j6.addLiving(pig);
		j6.addItem(stone);
		j6.addItem(minorHealth);
		
		starterHill.addLiving(pirate);
		starterHill.addLiving(pirate);
		starterHill.addLiving(pCaptain);
		starterHill.addLiving(pig);
		starterHill.addLiving(chicken);
		starterHill.addItem(helm);
		starterHill.addItem(bow);
		starterHill.addItem(majorHealth);
		
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pirate);
		tortugaS.addLiving(pCaptain);
		tortugaS.addLiving(pCaptain);
		tortugaS.addItem(stone);
		tortugaS.addItem(minorHealth);
		
		mission.addItem(iSword);
		mission.addLiving(navy);
		mission.addLiving(navy);
		mission.addLiving(navy);
		mission.addLiving(skel);
		mission.addItem(minorHealth);
		mission.addItem(minorHealth);
		
		graveyard.addLiving(Zomb);
		graveyard.addLiving(Zomb);
		graveyard.addLiving(Zomb);
		graveyard.addLiving(skel);
		graveyard.addLiving(pig);
		graveyard.addLiving(orc);
		graveyard.addLiving(orc);
		graveyard.addItem(corp);
		graveyard.addItem(minorHealth);
		
		tortugaW.addItem(stick);
		tortugaW.addItem(chest);
		tortugaW.addLiving(pirate);
		tortugaW.addLiving(navy);
		
		tortugaC.addLiving(navy);
		tortugaC.addLiving(nCaptain);
		tortugaC.addLiving(pirate);
		tortugaC.addLiving(pCaptain);
		tortugaC.addItem(minorHealth);
		tortugaC.addItem(MinorMana);
		
		tortugaN.addLiving(pig);
		tortugaN.addLiving(chicken);
		tortugaN.addLiving(pirate);
		tortugaN.addItem(stick);
		
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addLiving(pirate);
		bar.addItem(iGun);
		bar.addLiving(pCaptain);
		bar.addLiving(pCaptain);
		
		j11.addItem(iChest);
		j11.addLiving(orc);
		j11.addLiving(skel);
		
		burning1.addItem(iShield);
		
		burning2.addItem(stone);
		
		burningCave.addItem(stick);
		
		tortugaE.addLiving(pirate);
		tortugaE.addLiving(pirate);
		tortugaE.addLiving(navy);
		tortugaE.addLiving(navy);
		
		shop.addLiving(nCaptain);
		shop.addLiving(navy);
		shop.addLiving(navy);
		shop.addItem(iHelm);
		shop.addItem(iShoe);
		
		warehouse.addLiving(navy);
		warehouse.addLiving(pig);
		warehouse.addLiving(pig);
		
		docks3.addLiving(navy);
		docks3.addLiving(nCaptain);
		docks3.addItem(iSpear);
		
		empire.addLiving(nCaptain);
		empire.addLiving(nCaptain);
		empire.addLiving(nCaptain);
		empire.addItem(mSword);
		
		docks2.addLiving(pirate);
		docks2.addLiving(pirate);
		docks2.addLiving(pCaptain);
		docks2.addItem(iBow);
		
		dutchman.addLiving(pCaptain);
		dutchman.addLiving(pCaptain);
		dutchman.addLiving(pirate);
		dutchman.addLiving(davy);
		dutchman.addItem(mGun);
		
		docks1.addLiving(pirate);
		docks1.addLiving(orc);
		docks1.addItem(corp);
		docks1.addLiving(chicken);
		
		pier.addLiving(kraken);
		pier.addItem(corp);
		pier.addItem(corp);
		pier.addItem(corp);
		pier.addItem(corp);
		
		tortugaBurn.addLiving(Zomb);
		tortugaBurn.addLiving(Zomb);
		tortugaBurn.addLiving(skel);
		
		cellar.addItem(corp);
		cellar.addItem(corp);
		cellar.addItem(majorHealth);
		cellar.addItem(majorHealth);
		cellar.addItem(majorHealth);
		
		clockTower.addItem(mHelm);
		clockTower.addItem(majorHealth);
		clockTower.addItem(majorHealth);
		clockTower.addItem(majorHealth);
		
		manor.addItem(mGun);
		manor.addItem(stone);
		
		j21.addLiving(pig);
		j21.addLiving(pig);
		j21.addLiving(pig);
		
		j22.addLiving(pig);
		j22.addLiving(chicken);
	}

}
